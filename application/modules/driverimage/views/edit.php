<div class="page-header">
	<h1>
		<?php echo $title;?>
		<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		<?php echo $title;?>
	</small>
	</h1>

</div><!-- /.page-header -->
<div class="col-xs-12 col-sm-6">
<?php echo form_open_multipart('driverimage/add/'.$driverimage->ID_Image);?>
<div class="row">
<div class="form-group col-sm-6">
<label>	Driver <span style="color:red;"><?php echo form_error('ID_Driver');?></label>
<select name="ID_Driver" class="form-control">
<option value="">Select Driver</option>
<?php foreach($drivers as $row):?>
<option value="<?php echo $row->ID_Driver;?>" <?php if($row->ID_Driver==$driverimage->ID_Driver){ echo 'selected';}?>><?php echo $row->FirstName.' '.$row->LastName;?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-sm-6">
<label>Image Type <span style="color:red;"><?php echo form_error('ImageType');?></label>
<select name="ImageType" class="form-control">
<option value="">Select Type</option>
<option value="1" <?php if($driverimage->ImageType==1){ echo 'selected';}?>>Driver Photo</option>
<option value="2" <?php if($driverimage->ImageType==2){ echo 'selected';}?>>Driving License</option>
<option value="3" <?php if($driverimage->ImageType==3){ echo 'selected';}?>>Badge</option>
<option value="4" <?php if($driverimage->ImageType==4){ echo 'selected';}?>>CRBCheck</option>
</select>
</div>
<!-- <div class="form-group col-sm-12">
<label>	Image Sequence <span style="color:red;"><?php echo form_error('ImageSequence');?></label>
<input type="number" class="form-control" value="1" value="<?php echo $driverimage->ImageSequence; ?>" name="ImageSequence" />
</div> -->
</div>
<div class="form-group">
<label>	Image <span style="color:red;"><?php echo form_error('Image');?></label>
<input type="file" name="Image">
<?php if($driverimage->Image):?>
<img style="margin-top:7px;" src="<?php echo base_url();?>assets/images/driverimage/<?php echo $driverimage->Image;?>" width="100">
<?php endif;?>
<input type="hidden"  name="image" value="<?php echo $driverimage->Image;?>">
</div>
<div class="col-xs-12 row">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>driverimage" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>


</div>
</form>
</div>
















