<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Driverimage_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_Image';
    protected $_table = 'driverimage';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'ImageType',
            'label' => 'Image Type',
            'rules' => 'required|trim'
        ),array(
            'field' => 'ID_Driver',
            'label' => 'Driver',
            'rules' => 'required|trim'
        )
            //,array(
        //     'field' => 'ImageSequence',
        //     'label' => 'Image Sequence',
        //     'rules' => 'required|trim'
        // )
    );
    public function get_driverimage($id=''){
        if ($id) {
            $this->db->where('di.ID_Driver', $id);
        }
        $query=$this->db->select('*')->from('driverimage as di')
                        ->join('driver as d','d.ID_Driver=di.ID_Driver','left')
                        ->get();
                        return $query->result();
    }
}
