<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Driverimage extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('driverimage_m');
        $this->load->model('drivers/drivers_m');
    }

    public function index() {
        $data['view'] = "driverimage/home";
        $data['title'] = "Driver Images";
        if($this->session->userdata("AccessLevel") == 3) 
        {
        $data['driverimage'] = $this->driverimage_m->get_driverimage($this->session->userdata("user_id"));
        }
        else
        {
        $data['driverimage'] = $this->driverimage_m->get_driverimage();
        }
        $this->load->view('theme/layout', $data);
    }
    
    public function add($id='') {
    	 $this->load->library("form_validation");
    	 $rules = $this->driverimage_m->rules_add;
    	 $this->form_validation->set_rules($rules);
         if ($this->form_validation->run() == TRUE) {
        $data=array(
            'ImageType' =>$this->input->post('ImageType'),
            'ID_Driver' =>$this->input->post('ID_Driver'),
            'ImageSequence' =>$this->input->post('ImageSequence')
        	);

        if (file_exists($_FILES['Image']['tmp_name'])) {
         $img_response = $this->add_image();
         $data['Image'] = $img_response['name'].'_o.'.$img_response['ext'];
        }
        if($id){
        if (file_exists($_FILES['Image']['tmp_name'])) {
        $image = $this->input->post('image');
        unlink(FCPATH.'/assets/images/driverimage/'.$image);
        }
        $this->driverimage_m->update($id,$data);
		$image_detail = $this->driverimage_m->get($id);
	    $driver_id = $image_detail->ID_Driver; 
		$update_data['Update_Status'] = 3;
		$this->drivers_m->update($driver_id , $update_data);
        $this->session->set_flashdata('item','Successfully Updated!');
        }
        else
        {
        $insert_id = $this->driverimage_m->insert($data);
		$image_detail = $this->driverimage_m->get($insert_id);
		$driver_id = $image_detail->ID_Driver;
		$update_data['Update_Status'] = 3;
		$this->drivers_m->update($driver_id , $update_data);
        $this->session->set_flashdata('item','Successfully Added!');
        }
        
        redirect('driverimage');
	    }
	    else
	    {
		if($this->session->userdata("AccessLevel") == 3) { 
			 $data['drivers'] = $this->drivers_m->get_many_by("ID_Driver" , $this->session->userdata("user_id"));	
		} else { 
        $data['drivers'] = $this->drivers_m->get_many_by("ID_Company" , $this->session->userdata("ID_Company"));
		}
	    if ($id) {
        $data['driverimage'] = $this->driverimage_m->get($id);
        $data['view'] = "driverimage/edit";
        $data['title'] = "Edit Image";
        }
        else
        {
        $data['view'] = "driverimage/add";
        $data['title'] = "Add Image";  
        }
        $this->load->view('theme/layout', $data);
	    }
    

}
public function add_image() {
        $time = time() . rand();
        $post_image = array(
            'file' => $_FILES["Image"],
            'new_name' => $time . '_o',
            'dst_path' => FCPATH.'/assets/images/driverimage/'
        );
        $post_image_response = upload_original($post_image);
        //$src_path = FCPATH.'/assets/images/driverimage/'. $post_image_response['file_name'];
  
  //       $thumbs = array(
  //           array(
  //               'src_path' => $src_path,
  //               'dst_path' => FCPATH.'/assets/images/driverimage',
  //                'image_x' => 100,
  //               'image_y' => 100,
  //               'image_ratio' => FALSE,
  //               'quality' => '100%',
  //               'image_ratio_fill' => FALSE,
  //               'new_name' => $time . '_s'
  //           )
  //       ); 
  // if(empty($offer_image)) { 
  //  foreach ($thumbs as $thumb) {
  //   upload_resized_images($thumb);
  //  }
  // }
        return array('name' => $time, 'ext' => $post_image_response['file_ext']);
    }
public function delete($id='') {
        $detail = $this->driverimage_m->get($id);
        // unlink(FCPATH.'/assets/images/driverimage/'.$detail->image."_s.".$detail->flight_image_ext);
        unlink(FCPATH.'/assets/images/driverimage/'.$detail->image);
        $this->driverimage_m->delete($id);
        redirect('driverimage');
    }


}