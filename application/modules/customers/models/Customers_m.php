<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Customers_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_Customer';
    protected $_table = 'customer';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'DateOfBirth',
            'label' => 'Date Of Birth',
            'rules' => 'required|trim'
        ), array(
            'field' => 'FirstName',
            'label' => 'First Name',
            'rules' => 'required|trim'
        ), array(
            'field' => 'LastName',
            'label' => 'Last Name',
            'rules' => 'required|trim'
        ), array(
            'field' => 'Email',
            'label' => 'Email',
            'rules' => 'required|trim|valid_email'
        ), array(
            'field' => 'TelephoneNo',
            'label' => 'Telephone No',
            'rules' => 'required|trim'
        ), array(
            'field' => 'Gender',
            'label' => 'Gender',
            'rules' => 'required|trim'
        ), array(
            'field' => 'RegistrationMethod',
            'label' => 'Registration Method',
            'rules' => 'required|trim'
        ), array(
            'field' => 'BarredReason',
            'label' => 'Barred Reason',
            'rules' => 'required|trim'
        )
    );
    

}
