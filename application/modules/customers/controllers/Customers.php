<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('customers_m');
        $this->load->model('reason/reason_m');
    }

    public function index() {
        $data['view'] = "customers/home";
        $data['title'] = "Customers";
        $data['customers'] = $this->customers_m->get_all();
        $this->load->view('theme/layout', $data);
    }
    public function add($id='') {
         $this->load->library("form_validation");
         $rules = $this->customers_m->rules_add;
         $this->form_validation->set_rules($rules);
         //$data['module'] = "customers";
         if ($this->form_validation->run() == TRUE) {
        $data=array(
            'DateOfBirth' =>$this->input->post('DateOfBirth'),
            'FirstName' =>$this->input->post('FirstName'),
            'LastName' =>$this->input->post('LastName'),
            'Email' =>$this->input->post('Email'),
            'TelephoneNo' =>$this->input->post('TelephoneNo'),
            'Gender' =>$this->input->post('Gender'),
            'BarredReason' =>$this->input->post('BarredReason'),
            'PID' =>$this->input->post('PID'),
            'ID_Company' => $this->session->userdata("ID_Company")
            );
			
			if ($this->input->post('Password')) {
               // $data['Password']=md5($this->input->post('Password'));
            }

        if($id){
			$this->customers_m->update($id,$data);
			$this->session->set_flashdata('item','Successfully Updated!');
        }
        else
        {
			$this->customers_m->insert($data);
			$this->session->set_flashdata('item','Successfully Added!');
        }
        
			redirect('customers');
        }
        else
        {
        if ($id) {
        $data['customer'] = $this->customers_m->get($id);
        // $data['athority'] = $this->athorities_m->get($id);
        $data['view'] = "customers/edit";
        $data['title'] = "Edit Customer";
        }
        else
        {
        $data['view'] = "customers/add";
        $data['title'] = "Add Customer";  
        }
        $data['reason'] = $this->reason_m->get_all();
        $this->load->view('theme/layout', $data);
        }
    	
    }
    
public function delete($id=''){
    	$this->customers_m->delete($id);
    	redirect('customers');
    }

}
