<div class="page-header">
	<h1>
		<?php echo $title;?>
		<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									<?php echo $title;?>
								</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
<?php echo form_open('customers/add/'.$customer->ID_Customer);?>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	First Name <span style="color:red;"><?php echo form_error('FirstName');?></label>
<input type="text" class="form-control" value="<?php echo $customer->FirstName ;?>" required name="FirstName" placeholder="First Name">
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Last Name <span style="color:red;"><?php echo form_error('LastName');?></label>
<input type="text" class="form-control" value="<?php echo $customer->LastName ;?>" required name="LastName" placeholder="Last Name">
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Email <span style="color:red;"><?php echo form_error('Email');?></label>
<input type="email" class="form-control" value="<?php echo $customer->Email ;?>" required name="Email" placeholder="Email">
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Password <span style="color:red;"><?php echo form_error('Password');?></label>
<input type="password" class="form-control"   name="Password" placeholder="Password">
</div>
<div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Telephone<span style="color:red;"><?php echo form_error('TelephoneNo');?></label>
<div class="input-group">
<span class="input-group-addon">
<i class="ace-icon fa fa-phone"></i>
</span>
<input type="tel" class="form-control" value="<?php echo $customer->TelephoneNo ;?> " name="TelephoneNo" placeholder="Telephone">
</div>
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Date Of Birth <span style="color:red;"><?php echo form_error('DateOfBirth');?></label>
<div class="input-group">
<input type="text" class="form-control datetimepicker" value="<?php echo $customer->DateOfBirth ;?>"  name="DateOfBirth" placeholder="Date Of Birth">
<span class="input-group-addon">
<i class="fa fa-clock-o bigger-110"></i>
</span>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12  col-sm-4">
<label  for="form-field-1">Gender <span style="color:red;"><?php echo form_error('Gender');?></label>
<select name="Gender"  class="form-control">
<option value="0" <?php if($customer->Gender==0){echo 'selected';}?>>Unknown</option>
<option value="1" <?php if($customer->Gender==1){echo 'selected';}?>>Male</option>
<option value="2" <?php if($customer->Gender==2){echo 'selected';}?>>Female</option>
</select>
</div>

<div class="form-group col-xs-12  col-sm-4">
<label  for="form-field-1">Registration Method <span style="color:red;"><?php echo form_error('RegistrationMethod');?></label>
<select name="RegistrationMethod"  class="form-control">
<option value="0" <?php if($customer->RegistrationMethod==0){echo 'selected';}?>>None</option>
<option value="1" <?php if($customer->RegistrationMethod==1){echo 'selected';}?>>BidTaxi</option>
<option value="2" <?php if($customer->RegistrationMethod==2){echo 'selected';}?>>Facebook</option>
<option value="3" <?php if($customer->RegistrationMethod==3){echo 'selected';}?>>Twitter</option>
<option value="4" <?php if($customer->RegistrationMethod==4){echo 'selected';}?>>Google</option>
<option value="5" <?php if($customer->RegistrationMethod==5){echo 'selected';}?>>AppleID</option>
</select>
</div>
<div class="form-group col-xs-12  col-sm-4">
<label  for="form-field-1"> Barred Reason<span style="color:red;"><?php echo form_error('BarredReason');?></label>
<select name="BarredReason"  class="form-control">
<option value="" >None</option>
<?php foreach($reason as $row):?>
<option value="<?php echo $row->ID_Reason;?>" <?php if($row->ID_Reason==$customer->BarredReason){echo 'selected';} ;?>  ><?php echo $row->ReasonText;?></option>
<?php endforeach;?>
</select>
</div>
</div>
</div>
<div class="col-xs-12">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>customers" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>


</div>

</form>
</div>

