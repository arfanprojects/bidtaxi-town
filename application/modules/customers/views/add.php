<div class="page-header">
	<h1>
		<?php echo $title;?>
		<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									<?php echo $title;?>
								</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
<?php echo form_open('customers/add');?>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	First Name <span style="color:red;"><?php echo form_error('FirstName');?></label>
<input type="text" class="form-control" required name="FirstName" placeholder="First Name" id="form-field-1">
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Last Name <span style="color:red;"><?php echo form_error('LastName');?></label>
<input type="text" class="form-control" required name="LastName" placeholder="Last Name" id="form-field-1">
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Email <span style="color:red;"><?php echo form_error('Email');?></label>
<input type="email" class="form-control" required name="Email" placeholder="Email" id="form-field-1">
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Password <span style="color:red;"><?php echo form_error('Password');?></label>
<input type="password" class="form-control" required name="Password" placeholder="Password" id="form-field-1">
</div>
<div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Telephone <span style="color:red;"><?php echo form_error('TelephoneNo');?></label>
<div class="input-group">
	<span class="input-group-addon">
<i class="ace-icon fa fa-phone"></i>
</span>
<input type="tel" class="form-control"  name="TelephoneNo" placeholder="Telephone" id="form-field-1">
</div>
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Date Of Birth <span style="color:red;"><?php echo form_error('DateOfBirth');?></label>
<div class="input-group">
<input type="text" class="form-control datetimepicker"  name="DateOfBirth" placeholder="Date Of Birth" id="form-field-1">
<span class="input-group-addon">
	<i class="fa fa-clock-o bigger-110"></i>
</span>
</div>
</div>
</div>
</div>
<div class=" col-xs-12 col-md-6">
<div class='row'>
<div class="form-group col-xs-12 col-md-3  col-sm-4">
<label  for="form-field-1">Gender <span style="color:red;"><?php echo form_error('Gender');?></label>
<select name="Gender"  class="form-control">
<option value="0" >Unknown</option>
<option value="1">Male</option>
<option value="2">Female</option>
</select>
</div>

<div class="form-group col-xs-12  col-sm-4">
<label  for="form-field-1">Registration Method <span style="color:red;"><?php echo form_error('RegistrationMethod');?></label>
<select name="RegistrationMethod"  class="form-control">
<option value="0" >None</option>
<option value="1">BidTaxi</option>
<option value="2">Facebook</option>
<option value="3">Twitter</option>
<option value="4">Google</option>
<option value="5">AppleID</option>
</select>
</div>
<div class="form-group col-xs-12  col-sm-5">
<label  for="form-field-1"> Barred Reason<span style="color:red;"><?php echo form_error('BarredReason');?></label>
<select name="BarredReason"  class="form-control">
<option value="" >None</option>
<?php foreach($reason as $row):?>
<option value="<?php echo $row->ID_Reason;?>" ><?php echo $row->ReasonText;?></option>
<?php endforeach;?>
</select>
</div>
</div>
</div>
<div class="col-xs-12">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>customers" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>


</div>

</form>
</div>

