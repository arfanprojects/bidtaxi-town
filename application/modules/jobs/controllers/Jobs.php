<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('jobs_m');
        $this->load->model('customers/customers_m');
        $this->load->model('drivers/drivers_m');
        $this->load->model('cars/cars_m');
        $this->load->model('vehicletype/vehicletype_m');
    }

    public function index() {
        $data['view'] = "jobs/home";
        $data['title'] = "Jobs";
		if($this->session->userdata("AccessLevel") == 3) {
			$data['jobs'] = $this->jobs_m->get_many_by("ID_Driver" ,$this->session->userdata("user_id"));
		} else { 
			$data['jobs'] = $this->jobs_m->get_many_by("ID_Company" ,$this->session->userdata("ID_Company"));
		}
        $this->load->view('theme/layout', $data);
    }
    public function add($id='') {
         $this->load->library("form_validation");
         $rules = $this->jobs_m->rules_add;
         $this->form_validation->set_rules($rules);
         $data['customer'] = $this->customers_m->get_all();
         $data['driver'] = $this->drivers_m->get_many_by("ID_Company" , $this->session->userdata("ID_Company"));
         $data['car'] = $this->cars_m->get_all();
         $data['vehicle_type'] = $this->vehicletype_m->get_all();
          
         if ($this->form_validation->run() == TRUE) {
        $data=$this->jobs_m->array_from_post(array(
            'Status','AddressFrom','AddressTo','CancelledReason','ID_Customer','ID_Driver','ID_Car','BookedToLatitude','CurrentLatitude','CurrentLongitude',
            'DriverRating','CustomerRating','TelephoneNumber',
            'VehicleCategory','Multistop','DateTimeBooked','BookedFromLatitude',
            'BookedFromLongitude','BookedToLongitude','DateTimeArrived','ArrivedLatitude','ArrivedLongitude',
            'DateTimePOB','POBLatitude','POBLongitude','DateTimeComplete','CompleteLatitude','CompleteLongitude',
            'PassengerQuantity','WaitingTime','OnRouteTime','PriceBid','PriceAmendedBid','PriceTip','Mileage'

            ));
			
			$data['ID_Company'] = $this->session->userdata("ID_Company");
			
			

        if($id){
        $this->jobs_m->update($id,$data);
        $this->session->set_flashdata('item','Successfully Updated!');
        }
        else
        {
        $this->jobs_m->insert($data);
        $this->session->set_flashdata('item','Successfully Added!');
        }
        
        redirect('jobs');
        }
        else
        {
		 
        $data['title'] = "Add Job";
        $data['job'] = $this->jobs_m->default_object();
        if ($id) {
        $data['job'] = $this->jobs_m->get($id);
        $data['title'] = "Edit Job";
        }
        $data['view'] = "jobs/add";
        $this->load->view('theme/layout', $data);
        }
		
		
    	
    }
    
public function delete($id=''){
    	$this->jobs_m->delete($id);
    	redirect('jobs');
    }

}
