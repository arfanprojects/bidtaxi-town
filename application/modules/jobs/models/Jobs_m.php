<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Jobs_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_Job';
    protected $_table = 'job';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'Status',
            'label' => 'Job Status',
            'rules' => 'required|trim'
        ), array(
            'field' => 'AddressFrom',
            'label' => 'Address From',
            'rules' => 'required|trim'
        ), array(
            'field' => 'AddressTo',
            'label' => 'Address To',
            'rules' => 'required|trim'
        ), array(
            'field' => 'ID_Driver',
            'label' => 'Driver',
            'rules' => 'required|trim'
        )/*, array(
            'field' => 'ID_Car',
            'label' => 'Car',
            'rules' => 'required|trim'
        ), array(
            'field' => 'TelephoneNumber',
            'label' => 'Telephone',
            'rules' => 'required|trim'
        ), array(
            'field' => 'VehicleCategory',
            'label' => 'Vehicle Category',
            'rules' => 'required|trim'
        ) */

    );

public function default_object(){

    $job=new stdClass();
    $job->ID_job='';
    $job->Status='';
    $job->AddressFrom='';
    $job->AddressTo='';
    $job->CancelledReason='';
    $job->ID_Customer='';
    $job->ID_Driver='';
    $job->ID_Car='';
    $job->BookedToLatitude='';
    $job->CurrentLatitude='';
    $job->CurrentLongitude='';
    $job->DriverRating='';
    $job->CustomerRating='';
    $job->TelephoneNumber='';
    $job->VehicleCategory='';
    $job->Multistop='';
    $job->DateTimeBooked='';
    $job->BookedFromLatitude='';
    $job->BookedFromLongitude='';
    $job->BookedToLongitude='';
    $job->DateTimeArrived='';
    $job->ArrivedLatitude='';
    $job->ArrivedLongitude='';
    $job->DateTimePOB='';
    $job->POBLatitude='';
    $job->POBLongitude='';
    $job->DateTimeComplete='';
    $job->CompleteLatitude='';
    $job->CompleteLongitude='';
    $job->PassengerQuantity='';
    $job->WaitingTime='';
    $job->OnRouteTime='';
    $job->PriceBid='';
    $job->PriceAmendedBid='';
    $job->PriceTip='';
    $job->Mileage='';
    return $job;
}


}
