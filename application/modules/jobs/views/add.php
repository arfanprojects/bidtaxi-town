 <div class="page-header">
	<h1>
		<?php echo $title;?>
		<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		<?php echo $title;?>
	</small>
	</h1>

</div> <!-- /.page-header -->
<div class="col-xs-12 col-sm-12">
<?php  echo form_open('jobs/add/'.$job->ID_Job);?>

<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Status <span style="color:red;"><?php echo form_error('Status');?></span></label>
<select name="Status"  class="form-control">
<option value="">Select Status</option>
<option value="0" <?php if($job->Status==0){ echo 'selected';}?>>Outstanding</option>
<option value="1" <?php if($job->Status==1){ echo 'selected';}?>>Offered</option>
<option value="2" <?php if($job->Status==2){ echo 'selected';}?>>On Route</option>
<option value="3" <?php if($job->Status==3){ echo 'selected';}?>>Passenger on Board</option>
<option value="4" <?php if($job->Status==4){ echo 'selected';}?>>Complete</option>
<option value="5" <?php if($job->Status==5){ echo 'selected';}?>>No show</option>
<option value="6" <?php if($job->Status==6){ echo 'selected';}?>>Cancelled</option>
<option value="7" <?php if($job->Status==7){ echo 'selected';}?>>Bidding</option>
<option value="8" <?php if($job->Status==8){ echo 'selected';}?>>Soon to Arrive</option>
<option value="9" <?php if($job->Status==9){ echo 'selected';}?>>Arrived</option>
</select>
</div>

<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Address From <span style="color:red;"><?php echo form_error('AddressFrom');?></label>
<input type="text" class="form-control"  value="<?php echo $job->AddressFrom;?>"  name="AddressFrom" placeholder="Address From" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Address To <span style="color:red;"><?php echo form_error('AddressTo');?></label>
<input type="text" class="form-control"  value="<?php echo $job->AddressTo;?>"  name="AddressTo" placeholder="Address To" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Cancelled Reason <span style="color:red;"><?php echo form_error('CancelledReason');?></span></label>
<select name="CancelledReason" class="form-control">
<option value="">Select Reason</option>
<option value="1" <?php if($job->CancelledReason==1){ echo 'selected';}?>>By Customer</option>
<option value="2" <?php if($job->CancelledReason==2){ echo 'selected';}?>>By Driver</option>
<option value="3" <?php if($job->CancelledReason==3){ echo 'selected';}?>>Server Restrted</option>
</select>
</div>
<div class="form-group col-xs-12 col-md-4">
<label >Customer <span style="color:red;"><?php echo form_error('ID_Customer');?></span></label>
<select name="ID_Customer"  class="form-control">
<option value="">Select Customer</option>
<?php foreach($customer as $row):?>
<option value="<?php echo $row->ID_Customer;?>" <?php if($row->ID_Customer==$job->ID_Customer){ echo 'selected';}?>><?php echo $row->FirstName.' '.$row->LastName;?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-xs-12 col-md-4">
<label >Driver <span style="color:red;"><?php echo form_error('ID_Driver');?></span></label>
<select name="ID_Driver"  class="form-control">
<option value="">Select Driver</option>
<?php foreach($driver as $row):?>
<option value="<?php echo $row->ID_Driver;?>" <?php if($row->ID_Driver==$job->ID_Driver){ echo 'selected';}?>><?php echo $row->FirstName.' '.$row->LastName;?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-xs-12 col-md-4">
<label >Car <span style="color:red;"><?php echo form_error('ID_Car');?></span></label>
<select name="ID_Car"  class="form-control">
<option value="">Select Car</option>
<?php foreach($car as $row):?>
<option value="<?php echo $row->ID_Car;?>" <?php if($row->ID_Car==$job->ID_Car){ echo 'selected';}?>><?php echo $row->RegistrationPlate;?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-xs-12 col-md-4">
<label >Vehicle Category <span style="color:red;"><?php echo form_error('VehicleCategory');?></span></label>
<select name="VehicleCategory"  class="form-control">
<option value="">Select Category</option>
<?php foreach($vehicle_type as $row):?>
<option value="<?php echo $row->ID_VehicleType;?>" <?php if($row->ID_VehicleType==$job->VehicleCategory){ echo 'selected';}?>><?php echo $row->VehicleDescription;?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Multistop <span style="color:red;"><?php echo form_error('Multistop');?></span></label>
<select name="Multistop" class="form-control">
<option value="">Select Multistop</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
</select>
</div>

<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Booking Date <span style="color:red;"><?php echo form_error('DateTimeBooked');?></label>
<input type="datetime" class="form-control  datetimepicker" value="<?php echo $job->DateTimeBooked;?>"  name="DateTimeBooked" placeholder="Booking Date" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Booked From Latitude <span style="color:red;"><?php echo form_error('BookedFromLatitude');?></label>
<input type="text" class="form-control "   value="<?php echo $job->BookedFromLatitude;?>" name="BookedFromLatitude" placeholder="Booked From Latitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Booked From Longitude <span style="color:red;"><?php echo form_error('BookedFromLongitude');?></label>
<input type="text" class="form-control "   value="<?php echo $job->BookedFromLongitude;?>" name="BookedFromLongitude" placeholder="Booked From Longitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Booked To Latitude <span style="color:red;"><?php echo form_error('BookedToLatitude');?></label>
<input type="text" class="form-control "   value="<?php echo $job->BookedToLatitude;?>" name="BookedToLatitude" placeholder="Booked To Latitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Booked To Longitude <span style="color:red;"><?php echo form_error('BookedToLongitude');?></label>
<input type="text" class="form-control "   value="<?php echo $job->BookedToLongitude;?>" name="BookedToLongitude" placeholder="Booked To Longitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Current Latitude <span style="color:red;"><?php echo form_error('CurrentLatitude');?></label>
<input type="text" class="form-control "   value="<?php echo $job->CurrentLatitude;?>" name="CurrentLatitude" placeholder="Current Latitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Current Longitude <span style="color:red;"><?php echo form_error('CurrentLongitude');?></label>
<input type="text" class="form-control "   value="<?php echo $job->CurrentLongitude;?>" name="CurrentLongitude" placeholder="Current Longitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Date Arrived <span style="color:red;"><?php echo form_error('DateTimeArrived');?></label>
<input type="datetime" class="form-control datetimepicker" value="<?php echo $job->DateTimeArrived;?>"  name="DateTimeArrived" placeholder="Date Arrived" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Arrived Latitude <span style="color:red;"><?php echo form_error('ArrivedLatitude');?></label>
<input type="text" class="form-control" value="<?php echo $job->ArrivedLatitude;?>"  name="ArrivedLatitude" placeholder="Arrived Latitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Arrived Longitude <span style="color:red;"><?php echo form_error('ArrivedLongitude');?></label>
<input type="text" class="form-control" value="<?php echo $job->ArrivedLongitude;?>"  name="ArrivedLongitude" placeholder="Arrived Longitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Date POB <span style="color:red;"><?php echo form_error('DateTimePOB');?></label>
<input type="datetime" class="form-control datetimepicker" value="<?php echo $job->DateTimePOB;?>"  name="DateTimePOB" placeholder="Date POB" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	POB Latitude <span style="color:red;"><?php echo form_error('POBLatitude');?></label>
<input type="text" class="form-control" value="<?php echo $job->POBLatitude;?>"  name="POBLatitude" placeholder="POB Latitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	POB Longitude <span style="color:red;"><?php echo form_error('POBLongitude');?></label>
<input type="text" class="form-control" value="<?php echo $job->POBLongitude;?>"  name="POBLongitude" placeholder="POB Longitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Date Complete <span style="color:red;"><?php echo form_error('DateTimeComplete');?></label>
<input type="datetime" class="form-control datetimepicker" value="<?php echo $job->DateTimeComplete;?>"  name="DateTimeComplete" placeholder="Date Complete" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Complete Latitude <span style="color:red;"><?php echo form_error('CompleteLatitude');?></label>
<input type="text" class="form-control" value="<?php echo $job->CompleteLatitude;?>"  name="CompleteLatitude" placeholder="Complete Latitude" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label  for="form-field-1">	Complete Longitude <span style="color:red;"><?php echo form_error('CompleteLongitude');?></label>
<input type="text" class="form-control" value="<?php echo $job->CompleteLongitude;?>"  name="CompleteLongitude" placeholder="Complete Longitude" >
</div>

<div class="form-group col-xs-12 col-md-4">
<label>	Passenger Quantity</label>
<input type="number" class="form-control" value="<?php echo $job->PassengerQuantity;?>"  name="PassengerQuantity" placeholder="Passenger Quantity" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label>	Waiting Time</label>
<input type="text" class="form-control" value="<?php echo $job->WaitingTime;?>"  name="WaitingTime" placeholder="Waiting Time" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label>	On Route Time</label>
<input type="text" class="form-control" value="<?php echo $job->OnRouteTime;?>"  name="OnRouteTime" placeholder="On Route Time" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label>	Price Bid</label>
<input type="number" class="form-control"  value="<?php echo $job->PriceBid;?>"  name="PriceBid" placeholder="Price Bid" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label>	Price Amended Bid</label>
<input type="number" class="form-control" value="<?php echo $job->PriceAmendedBid;?>"  name="PriceAmendedBid" placeholder="Price Amended Bid" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label>	Price Tip</label>
<input type="number" class="form-control" value="<?php echo $job->PriceTip;?>"  name="PriceTip" placeholder="Price Tip" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label>	Mileage</label>
<input type="text" class="form-control" value="<?php echo $job->Mileage;?>"  name="Mileage" placeholder="Mileage" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label>	Driver Rating</label>
<input type="text" class="form-control" value="<?php echo $job->DriverRating;?>"  name="DriverRating" placeholder="Driver Rating" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label>	Customer Rating</label>
<input type="text" class="form-control" value="<?php echo $job->CustomerRating;?>"  name="CustomerRating" placeholder="Customer Rating" >
</div>
<div class="form-group col-xs-12 col-md-4">
<label>	Telephone</label>
<input type="text" class="form-control"  value="<?php echo $job->TelephoneNumber;?>"  name="TelephoneNumber" placeholder="Telephone" >
</div>

<div class="col-xs-12">
<div class=" col-md-3 row pull-right text-right">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>jobs" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>

</div>


</div>

</form>
</div>

