
<style type="text/css">

.status{
	border-radius: 3px;
    font-size: 15px;
    height: auto;
    padding: 6px 9px;
}
</style>
<?php if ($this->session->userdata('item')):?>
	<div class="alert alert-block alert-success col-xs-11">
	<button data-dismiss="alert" class="close" type="button">
		<i class="ace-icon fa fa-times"></i>
	</button>

	<i class="ace-icon fa fa-check green"></i>
	<?php echo $this->session->flashdata('item');?>
</div>
<?php endif;?>
						<div class="page-header col-xs-12">
							<h1>
								<?php echo $title;?>
								<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		<?php echo $title;?>
	</small>
							</h1>

						</div><!-- /.page-header -->
						<div class="row">
						<div class="col-xs-12">
							<div class="row">
									<div class="col-xs-12">
										<a style="margin:5px 6px;" href="<?php echo base_url();?>jobs/add" class="btn btn btn-primary pull-right"><i class="ace-icon glyphicon glyphicon-plus"></i> Add</a>
										<table class="table datatable table-bordered table-hover" id="simple-table">
											<thead>
												<tr>
													<th class="detail-col">ID</th>
													<th>Title</th>
													<th>Booking Date</th>
													<th>Status</th>
													<th>Options</th>
								</tr>
											</thead>

											<tbody>
												<?php foreach($jobs as $row):?>
												<tr>
													<td class=" bigger-120 center">
														
												<?php echo $row->ID_Job;?>
															
													</td>
													<td></td>
												<td class=" bigger-120 ">
														
														<?php echo $row->DateTimeBooked;?>	
													</td>
													
													
													<td class="bigger-120">
												<?php  

					if($row->Status==0){
						echo '<span class="label status label-sm label-success">Outstanding</span>';
					}
					else if($row->Status==1)
					{
					echo '<span class="label status label-sm label-info">Offered</span>';	
					}else if($row->Status==2)
					{
					echo '<span class="label status label-sm label-success">On Route</span>';	
					}else if($row->Status==3)
					{
					echo '<span class="label status label-sm label-success">Passenger on Board</span>';	
					}
					else if($row->Status==4)
					{
					echo '<span class="label status label-sm label-success">Complete</span>';	
					}else if($row->Status==5)
					{
					echo '<span class="label status label-sm label-warning">No show</span>';	
					}else if($row->Status==6)
					{
					echo '<span class="label status label-sm label-warning">Cancelled</span>';	
					}else if($row->Status==7)
					{
					echo '<span class="label status label-sm label-success">Bidding</span>';	
					}else if($row->Status==8)
					{
					echo '<span class="label status label-sm label-success">Soon to Arrive</span>';	
					}else if($row->Status==9)
					{
					echo '<span class="label status label-sm label-success">Arrived</span>';	
					}

					?>
													</td>
													<td>
														<div class=" btn-group action-buttons">
															<?php if($this->session->userdata("AccessLevel") != 3) { ?>
															<a class="green"  href="<?php echo base_url();?>jobs/add/<?php echo $row->ID_Job;?>">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</a>

															<a class="red confirm" href="<?php echo base_url();?>jobs/delete/<?php echo $row->ID_Job;?>">
																<i class="ace-icon fa fa-trash-o bigger-120"></i>
															</a>
															<?php } ?>
														</div>
													</td>
												</tr>
											<?php endforeach;?>
											</tbody>
										</table>
									</div><!-- /.span -->
								</div>

						</div>

						</div>