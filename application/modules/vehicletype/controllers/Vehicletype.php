<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicletype extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('vehicletype_m');
    }

    public function index() {
        $data['view'] = "vehicletype/home";
        $data['title'] = "Vehicle Type";
        $data['vehicletype'] = $this->vehicletype_m->get_all();
        $this->load->view('theme/layout', $data);
    }
    public function add($id='') {
    	if ($id) {
    	$data['vehicletype'] = $this->vehicletype_m->get($id);
    	$data['view'] = "vehicletype/edit";
        $data['title'] = "Edit Vehicle Type";
    	}
    	else
    	{
    	$data['view'] = "vehicletype/add";
        $data['title'] = "Add Vehicle Type";	
    	}
        $this->load->view('theme/layout', $data);
    }
    public function save($id='') {
    	 $this->load->library("form_validation");
    	 $rules = $this->vehicletype_m->rules_add;
    	 $this->form_validation->set_rules($rules);
         if ($this->form_validation->run() == TRUE) {
        $data=array(
        	'VehicleDescription' =>$this->input->post('VehicleDescription')
        	);
        if($id){
        $this->vehicletype_m->update($id,$data);
        $this->session->set_flashdata('item','Successfully Updated!');
        }
        else
        {
        $this->vehicletype_m->insert($data);
        $this->session->set_flashdata('item','Successfully Added!');
        }
        
        redirect('vehicletype');
	    }
	    else
	    {
	    	$data['view'] = "vehicletype/add";
	        $data['title'] = "Add Vehicle Type";
	        $this->load->view('theme/layout', $data);
	    }
    

}
public function delete($id=''){
    	$this->vehicletype_m->delete($id);
    	redirect('vehicletype');
    }

}