<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Vehicletype_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_VehicleType';
    protected $_table = 'vehicletype';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'VehicleDescription',
            'label' => 'Vehicle Description',
            'rules' => 'required|trim'
        )
    );
}
