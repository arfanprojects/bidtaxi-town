<div class="page-header">
	<h1>
		<?php echo $title;?>
		<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		<?php echo $title;?>
	</small>
	</h1>

</div><!-- /.page-header -->
<div class="col-xs-12 col-sm-6">
<?php echo form_open('vehicletype/save/'.$vehicletype->ID_VehicleType);?>
<div class="form-group">
<label  for="form-field-1">	Vehicle Description <span style="color:red;"><?php echo form_error('VehicleDescription');?></label>
<input type="text" class="form-control" required value="<?php echo $vehicletype->VehicleDescription;?>" name="VehicleDescription" placeholder="Vehicle Description">
</div>
<div class="col-xs-12 row">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>vehicletype" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>



</div></form>
</div>

