<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Drivers_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_Driver';
    protected $_table = 'driver';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'firstname',
            'label' => 'First Name',
            'rules' => 'required|trim'
        ), array(
            'field' => 'lastname',
            'label' => 'Last Name',
            'rules' => 'required|trim'
        ), array(
            'field' => 'gender',
            'label' => 'Gender',
            'rules' => 'required|trim'
        ), array(
            'field' => 'Email',
            'label' => 'Email',
            'rules' => 'required|trim'
        ),

        /*, array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'required|trim'
        ),
        //  array(
        //     'field' => 'country',
        //     'label' => 'Country',
        //     'rules' => 'required|trim'
        // )
          array(
            'field' => 'postcode',
            'label' => 'Post Code',
            'rules' => 'required|trim'
        ), array(
            'field' => 'nationality',
            'label' => 'Nationality',
            'rules' => 'required|trim'
        ), array(
            'field' => 'stdate',
            'label' => 'Start Date',
            'rules' => 'required|trim'
        ), array(
            'field' => 'fndate',
            'label' => 'Finish Date',
            'rules' => 'required|trim'
        )
        , array(
            'field' => 'car',
            'label' => 'Car',
            'rules' => 'required|trim'
        )
        , array(
            'field' => 'active',
            'label' => 'Active',
            'rules' => 'required|trim'
        ), array(
            'field' => 'crb',
            'label' => 'CRB Certification',
            'rules' => 'required|trim'
        ), array(
            'field' => 'drl',
            'label' => 'License Number',
            'rules' => 'required|trim'
        ), array(
            'field' => 'lexp',
            'label' => 'Expiry Date',
            'rules' => 'required|trim'
        ), array(
            'field' => 'BadgeNo',
            'label' => 'Badge No.',
            'rules' => 'required|trim'
        ), array(
            'field' => 'badgexp',
            'label' => 'Badge Expiry Date',
            'rules' => 'required|trim'
        ), array(
            'field' => 'PaymentExpiry',
            'label' => 'Payment Expiry Date',
            'rules' => 'required|trim'
        ), array(
            'field' => 'BarredExpiry',
            'label' => 'Barred Expiry Date',
            'rules' => 'required|trim'
        ) */




    );

public function car_detail($id=''){
    $this->db->where('ID_Driver',$id);
    $query=$this->db->get('car');
    $row = $query->row();
	
        $row->images=$this->get_images($row->ID_Car);
		
        $row->main_image=$this->get_main($row->ID_Car);
return $row;
}
public function get_images($id=''){
    $this->db->where('ID_Car',$id);
    $query=$this->db->get('carimage');
    return $query->result();
}
public function get_main($id=''){
    $this->db->where('ID_Car',$id);
    $query=$this->db->get('carimage');
    return $query->last_row();
}
public function get_company($id=''){
    $db1=$this->load->database('master', TRUE);
    $db1->where('ID_Company',$id);
    $query= $db1->get('companies');
    $company=$query->row();
    return $company->CompanyName;
}
public function get_country($id=''){
    $db1=$this->load->database('master', TRUE);
    $db1->where('ID_Country',$id);
    $query= $db1->get('country');
    $country=$query->row();
	if($country) {
		return $country->CountryText;
	} else { 
		return true;
	}
}
public function default_object(){

    $driver=new stdClass();
    $driver->ID_Driver='';
    $driver->FirstName='';
    $driver->LastName='';
    $driver->ID_Country='';
    $driver->ID_Company='';
    $driver->Email='';
    $driver->Telephone1='';
    $driver->Telephone2='';
    $driver->Address1='';
    $driver->Address2='';
    $driver->Address3='';
    $driver->Address4='';
    $driver->Gender='';
    $driver->Nationality='';
    $driver->PostCode='';
    $driver->Status='';
    $driver->StartDate='';
    $driver->FinishDate='';
    $driver->ID_Car='';
    $driver->ID_Job='';
    $driver->Active='';
    $driver->InactiveReason='';
    // $driver->Latitude='';
    // $driver->Longitude='';
    $driver->CRBExpiry=0;
    $driver->DrivingLicenceNo='';
    $driver->DrivingLicenceExpiry='';
    $driver->BadgeNo='';
    $driver->BadgeExpiry='';
    $driver->PaymentExpiry='';
    $driver->BarredExpiry='';
    $driver->CancelledReason='';
    $driver->BankAccountNo='';
    $driver->Update_Status='';
    $driver->AccountNumber='';
    return $driver;
}

}
