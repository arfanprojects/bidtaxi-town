<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Drivers extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('drivers_m');
        $this->load->model('cars/cars_m');
        $this->load->model('driverpayment/driverpayment_m');
        $this->load->model('driver_job/driver_job_m');
        $this->load->model('driverimage/driverimage_m');
        $this->load->model('driverbilling/driverbilling_m');
        $this->load->model('jobs/jobs_m');
        $this->load->model('reason/reason_m');

    }
   public function get_drivers($status){
    $drivers = $this->drivers_m->get_many_by(array("ID_Company" => $this->session->userdata("ID_Company") , "Update_Status" => $status));
        foreach ($drivers as $row) {
            $row->images=$this->driverimage_m->get_many_by('ID_Driver',$row->ID_Driver);
        }
        return $drivers;
   }
    public function index() {
	if($this->session->userdata("AccessLevel") == 3) {
			redirect("dashboard");
		}
        $data['view'] = "drivers/home";
        $data['title'] = "Active Drivers";
        $data['drivers']=$this->get_drivers(1);
        $this->load->view('theme/layout', $data);
    }

	 public function new_pending() {
	 if($this->session->userdata("AccessLevel") == 3) {
			redirect("dashboard");
		}
        $data['view'] = "drivers/home";
        $data['title'] = "New Drivers";
        $data['drivers'] = $this->get_drivers(2);
        $this->load->view('theme/layout', $data);
    }

	public function pending() {
	if($this->session->userdata("AccessLevel") == 3) {
			redirect("dashboard");
		}
        $data['view'] = "drivers/home";
        $data['title'] = "Pending Drivers";
        $data['drivers'] = $this->get_drivers(3);
        $this->load->view('theme/layout', $data);
    }
    public function add($id='') {

         $db1=$this->load->database('master', TRUE);
         $country= $db1->get_where('country',array("CountryType" => 1))->result();
         $company= $db1->get('companies')->result();
         $nationality = $db1->get_where('country',array("CountryType" => 2))->result();
         $this->load->library("form_validation");
         $rules = $this->drivers_m->rules_add;
         $this->form_validation->set_rules($rules);
         $data['nationalities'] = $nationality;
         $data['countries'] = $country;
         $data['companies'] = $company;
         $data['cars'] = $this->cars_m->get_many_by('ID_Company' , $this->session->userdata("ID_Company"));
         $data['jobs'] = $this->jobs_m->get_many_by('ID_Company' , $this->session->userdata("ID_Company"));
         $data['reason'] = $this->reason_m->get_many_by('ID_Company' , $this->session->userdata("ID_Company"));
         if ($this->form_validation->run() == TRUE) {
        $data=array(
            'FirstName' =>$this->input->post('firstname'),
            'LastName' =>$this->input->post('lastname'),
            'ID_Country' =>$this->input->post('country'),
            'ID_Company' =>$this->input->post('ID_Company'),
            'Email' =>$this->input->post('Email'),
            'Telephone1' =>$this->input->post('Telephone1'),
            'Telephone2' =>$this->input->post('Telephone2'),
            'Address1' =>$this->input->post('address1'),
            'Address2' =>$this->input->post('address2'),
            'Address3' =>$this->input->post('address3'),
            'Address4' =>$this->input->post('address4'),
            'Gender' =>$this->input->post('gender'),
            'Nationality' =>$this->input->post('nationality'),
            'PostCode' =>$this->input->post('postcode'),
            'Status' =>$this->input->post('status'),
            'StartDate' =>$this->input->post('stdate'),
            'FinishDate' =>$this->input->post('fndate'),
            'ID_Car' =>$this->input->post('car'),
           // 'ID_Job' =>$this->input->post('job'),
            'Active' =>$this->input->post('active'),
            'InactiveReason' =>$this->input->post('reason'),
            'BankAccountNo' =>$this->input->post('AccountNumber'),
            // 'Latitude' =>$this->input->post('latitude'),
            // 'Longitude' =>$this->input->post('longitude'),
            'CRBExpiry' =>$this->input->post('CRBExpiry'),
            'DrivingLicenceNo' =>$this->input->post('drl'),
            'DrivingLicenceExpiry' =>$this->input->post('lexp'),
            'BadgeNo' =>$this->input->post('BadgeNo'),
            'BadgeExpiry' =>$this->input->post('badgexp'),
            'PaymentExpiry' =>$this->input->post('PaymentExpiry'),
            'BarredExpiry' =>$this->input->post('BarredExpiry'),
            'InactiveReason' =>$this->input->post('CancelledReason')
            );

        if($id){
        $this->drivers_m->update($id,$data);

		if (file_exists($_FILES['upload_file1']['tmp_name'])) {
                foreach ($_FILES as $key => $value) {
                    if ($key != "file1") {
                        $img_res = $this->add_image($key);
                        $image_data = array(
                            'ID_Driver' => $id,
                            'Image' => $img_res['name'].'_o.'.$img_res['ext']
                        );
                        $this->driverimage_m->insert($image_data);
                    }
                }
            }

		$update_data['Update_Status'] = 3;
		$this->drivers_m->update($id , $update_data);

        $this->session->set_flashdata('item','Successfully Updated!');
        if($this->session->userdata("AccessLevel") == 3) {

          redirect("dashboard");
        }

        }
        else
        {
		// if($this->session->userdata("AccessLevel") == 3) {
    //
		// 	redirect("dashboard");
		// }
        $insert_id = $this->drivers_m->insert($data);

		if (file_exists($_FILES['upload_file1']['tmp_name'])) {
                foreach ($_FILES as $key => $value) {
                    if ($key != "file1") {
                        $img_res = $this->add_image($key);
                        $image_data = array(
                            'ID_Driver' => $insert_id,
                            'Image' => $img_res['name'].'_o.'.$img_res['ext']
                        );
                        $this->driverimage_m->insert($image_data);
                    }
                }
            }

        $this->session->set_flashdata('item','Successfully Added!');
        }

        redirect('drivers');
        }
        else
        {
        $data['title'] = "Add Driver";
        $data['driver'] = $this->drivers_m->default_object();
        if ($id) {
        $data['driver'] = $this->drivers_m->get($id);
		$data['images'] = $this->driverimage_m->get_many_by("ID_Driver" , $id);
        $data['title'] = "Edit Driver";
        }
        $data['view'] = "drivers/add";
        $this->load->view('theme/layout', $data);
        }

    }

public function delete($id=''){
		if($this->session->userdata("AccessLevel") == 3 || $this->session->userdata("AccessLevel") == 2) {
			redirect("dashboard");
		}
        $image=$this->driverimage_m->count_by('ID_Driver',$id);
        $cars=$this->cars_m->count_by('ID_Driver',$id);
        $jobs=$this->driver_job_m->count_by('ID_Driver',$id);
        $payment=$this->driverpayment_m->count_by('ID_Driver',$id);
        $billing=$this->driverbilling_m->count_by('ID_Driver',$id);
        $total=$image+$cars+$jobs+$payment+$billing;
        if ($total) {
           $this->session->set_flashdata('item', 'Cannot delete driver.There are some itmes associated with this driver eg cars,driver payment,jobs etc.Please delete them first?');
           redirect('drivers');
        }
    	$this->drivers_m->delete($id);
    	redirect('drivers');
    }

	function change_status() {
		$ID_Driver = $this->input->post("driver_id");
		$data['Update_Status'] = $this->input->post("status");
		$this->drivers_m->update($ID_Driver , $data);
		return TRUE;
	}
    function driver_detail($id='') {
       if ($id) {
        $row=$this->drivers_m->get($id);

        $row->company=$this->drivers_m->get_company($row->ID_Company);
        if ($row->ID_Country) {
        $row->country=$this->drivers_m->get_country($row->ID_Country);
        }
        else
        {
            $row->country='';
        }

        $data['driver'] = $row;
        $data['driverimages']=$this->driverimage_m->get_many_by('ID_Driver',$id);
        $data['car']=$this->drivers_m->car_detail($id);
        $data['payment']=$this->driverpayment_m->get_many_by('ID_Driver',$id);
        $data['billing']=$this->driverbilling_m->get_many_by('ID_Driver',$id);
        $data['title'] = "Driver Detail";
        $data['view'] = "drivers/detail";
        $this->load->view('theme/layout', $data);
        }

    }


	public function add_image($key) {
        $time = time() . rand();
        $post_image = array(
            'file' => $_FILES[$key],
            'new_name' => $time . '_o',
            'dst_path' => FCPATH.'/assets/images/driverimage/'
        );
        $post_image_response = upload_original($post_image);
        return array('name' => $time, 'ext' => $post_image_response['file_ext']);
    }

	public function deleteimage($id) {
		$carid = $this->uri->segment(4);
		$this->driverimage_m->delete($id);
		$update_data['Update_Status'] = 3;
		$this->drivers_m->update($carid , $update_data);

		redirect("drivers/add/" . $carid);
	}

}
