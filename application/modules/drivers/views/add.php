<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<!-- <div class="page-header">
	<h1>
		<?php echo $title;?>
	</h1>

</div> --><!-- /.page-header -->
<div class="col-xs-12 col-sm-12">



<?php  echo form_open_multipart('drivers/add/'.$driver->ID_Driver);?>
<script>
  $(function() {
  	var data='<?php echo $driver->CRBExpiry;?>';
  	if (data) {
  		$('#crb').bootstrapToggle('on');
  	}else{
  		$('#crb').bootstrapToggle('off');
  	}
	$('#crb').change(function() {
	  var id = $(this).prop('checked');
	  if (id) {
	  	$(this).val(1);
	  }else{
	  	$(this).val(0);
	  }
	});
});
</script>
<div class="col-xs-12 ">
	<fieldset>
	<legend class="color">Personal Detail</legend>

	<?php if(($this->session->userdata("AccessLevel") == 2 or $this->session->userdata("AccessLevel") == 1) and ($driver->Update_Status == 2 or  $driver->Update_Status == 3)) : ?>
					<script>
					  $(function() {
						$('#toggle-event').change(function() {
						  var id = $(this).prop('checked');
						  var driver_id = $(this).attr("data-id");
						  if(id) {
							var form_data = {
								status : 1,
								driver_id : driver_id
							}
						  } else {
							var form_data = {
								status : 3,
								driver_id : driver_id
							}
						  }

						  $.ajax({
							 type:"post",
							 data: form_data,
							 url: "<?php echo site_url("drivers/change_status"); ?>",
							 success : function(msg){
								location_reload();
							 }
						  });

						});
					  });
					</script>


					<input type="checkbox" id="toggle-event" data-id="<?php echo $driver->ID_Driver; ?>"  data-toggle="toggle" data-on="Active" data-off="Pending" data-onstyle="success" data-offstyle="danger">
				<?php endif; ?>

<div class="row">
	<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	First Name <span style="color:red;"><?php echo form_error('firstname');?></span></label>
<input type="text" name="firstname" value="<?php echo $driver->FirstName?>"  class="form-control" placeholder="First Name" autofocus="">
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Last Name <span style="color:red;"><?php echo form_error('lastname');?></label>
<input type="text" class="form-control"  value="<?php echo $driver->LastName?>" name="lastname" placeholder="Last Name" >
</div>
	<div class="form-group col-xs-12 col-md-6">
<label>	Your Email <span style="color:red;"><?php echo form_error('Email');?></label>
<input type="email" class="form-control" value="<?php echo $driver->Email?>"  name="Email" placeholder="Enter Email" >
</div>
<div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Telephone 1 <span style="color:red;"><?php echo form_error('Telephone1');?></label>
<div class="input-group">
<input type="tel" class="form-control" value="<?php echo $driver->Telephone1;?>"  name="Telephone1" required placeholder="Telephone1" id="form-field-1">
<span class="input-group-addon">
<i class="ace-icon fa fa-phone"></i>
</span>
</div>
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Telephone 2 <span style="color:red;"><?php echo form_error('Telephone2');?></label>
<div class="input-group">
<input type="tel" class="form-control" value="<?php echo $driver->Telephone2;?>" name="Telephone2" placeholder="Telephone2">
<span class="input-group-addon">
<i class="ace-icon fa fa-phone"></i>
</span>
</div>
</div>
</div>
</div>
<div class="form-group col-xs-12 col-md-2">
<label>Gender <span style="color:red;"><?php echo form_error('gender');?></label>
<select class="form-control" name="gender">
<option value="0" <?php if($driver->Gender==0){ echo 'selected';}?>>Not known</option>
<option value="1" <?php if($driver->Gender==1){ echo 'selected';}?>>Male</option>
<option value="2" <?php if($driver->Gender==2){ echo 'selected';}?>>Female</option>
</select>
</div>
<div class="form-group col-xs-12 col-md-4">
<label>Nationality</label>
<select class="form-control" name="nationality" required="">
<option value="">Select Nationality</option>
<?php foreach($nationalities as $row):?>
<option value="<?php echo $row->ID_Country?>" <?php if($row->ID_Country==$driver->Nationality){ echo 'selected';}?>><?php echo $row->CountryText;?></option>
<?php endforeach;?>
</select>
</div>
</div>
</fieldset>
</div>
<div class="col-xs-12">
<fieldset>
<legend class="color">Address</legend>
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Address 1 <span style="color:red;"><?php echo form_error('address1');?></label>
<input type="text" class="form-control" value="<?php echo $driver->Address1;?>"   name="address1" placeholder="House/Street" >
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Address 2 <span style="color:red;"><?php echo form_error('address2');?></label>
<input type="text" class="form-control"  name="address2" value="<?php echo $driver->Address2;?>"  placeholder="Address2" >
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Address 3 <span style="color:red;"><?php echo form_error('address3');?></label>
<input type="text" class="form-control" value="<?php echo $driver->Address3;?>"   name="address3" placeholder="Address3" >
</div>

<div class="col-xs-12 col-md-6">
<label  for="form-field-1">	Address 4 <span style="color:red;"><?php echo form_error('address4');?></label>
<input type="text" class="form-control" value="<?php echo $driver->Address4;?>"  name="address4" placeholder="Town or City" >
</div>
<div class="clearfix"></div>
<div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Country <span style="color:red;"><?php echo form_error('ID_Country');?></label><br>
<select name="country" class="form-control" required="">
<option value="">Select Country</option>
<?php foreach($countries as $row):?>
<option value="<?php echo $row->ID_Country?>" <?php if($row->ID_Country==$driver->ID_Country){ echo 'selected';}?>><?php echo $row->CountryText;?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">Company <span style="color:red;"><?php echo form_error('ID_Company');?></label><br>
<select name="ID_Company" class="form-control">
<option value="">Select Company</option>
<?php foreach($companies as $row):?>
<option value="<?php echo $row->ID_Company?>" <?php if($row->ID_Company==$driver->ID_Company){ echo 'selected';}?>><?php echo $row->CompanyName;?></option>
<?php endforeach;?>
</select>
</div>
</div>
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Post Code <span style="color:red;"><?php echo form_error('postcode');?></label>
<input type="text" class="form-control" value="<?php echo $driver->PostCode?>" required name="postcode" placeholder="Post Code" >
</div>
</fieldset>
</div>

<div class="col-xs-12">
<fieldset>
<legend class="color">Other</legend>
<div class="row">
<div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Start Date <span style="color:red;"><?php echo form_error('stdate');?></label>
<input type="datetime" class="form-control datetimepicker" value="<?php echo $driver->StartDate;?>"  name="stdate" placeholder="Start Date" >
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Finish Date <span style="color:red;"><?php echo form_error('fndate');?></label>
<input type="datetime" class="form-control datetimepicker"   value="<?php echo $driver->FinishDate;?>" name="fndate" placeholder="Finish Date" >
</div>
</div>
</div>
<div class="col-xs-12 col-md-6">
<div class="row">
	<div class="form-group col-xs-12 col-md-6">
<label>Car </label>
<select title="Car currently driving" class="form-control" name="car">
<option value="">Select car</option>
<?php foreach($cars as $row):?>
<option value="<?php echo $row->ID_Car?>" <?php if($row->ID_Car==$driver->ID_Car){ echo 'selected';}?>><?php echo $row->RegistrationPlate ;?></option>
<?php endforeach;?>
</select>
</div>
<!-- <div class="form-group col-xs-12 col-md-4">
<label>Job </label>
<select title="Job currently on" class="form-control" name="job">
<option value="">Select Job</option>
<?php foreach($jobs as $row):?>
<option value="<?php echo $row->ID_Job?>" <?php if($row->ID_Job==$driver->ID_Job){ echo 'selected';}?>>job</option>
<?php endforeach;?>
</select>
</div>
 -->
 <div class="form-group col-xs-12 col-md-6">
<label>Status </label>
<select title="Status" class="form-control" name="status">
<option value="">Select Status</option>
<option value="0" <?php if($driver->Status==0){ echo 'selected';}?>>Off</option>
<option value="1" <?php if($driver->Status==1){ echo 'selected';}?>>Scrub</option>
<option value="2" <?php if($driver->Status==2){ echo 'selected';}?>>Clear</option>
<option value="3" <?php if($driver->Status==3){ echo 'selected';}?>>Bidding</option>
<option value="4" <?php if($driver->Status==4){ echo 'selected';}?>>OnRoute</option>
<option value="5" <?php if($driver->Status==5){ echo 'selected';}?>>Soon to Arrive</option>
<option value="6" <?php if($driver->Status==6){ echo 'selected';}?>>Arrived</option>
<option value="7" <?php if($driver->Status==7){ echo 'selected';}?>>POB</option>
<option value="8" <?php if($driver->Status==8){ echo 'selected';}?>>Confirm</option>
<option value="9" <?php if($driver->Status==9){ echo 'selected';}?>>Busy</option>
<option value="10" <?php if($driver->Status==10){ echo 'selected';}?>>Undefined</option>
<option value="11" <?php if($driver->Status==11){ echo 'selected';}?>>Driver Paid</option>
</select>
</div>
</div>
</div>
<div class="clearfix"></div>
<div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label>Active</label>
<select  class="form-control" name="active">
<option value="1" <?php if($driver->Active==1){ echo 'selected';}?>>Active</option>
<option value="0" <?php if($driver->Active==0){ echo 'selected';}?>>Inactive</option>
<option value="2" <?php if($driver->Active==2){ echo 'selected';}?>>Pending</option>
</select>
</div>
<div class="form-group col-xs-12 col-md-6">
<label>Reason</label>
<select  class="form-control"  name="CancelledReason">
<option value="">Select Reason</option>
<?php foreach($reason as $row):?>
<option value="<?php echo $row->ID_Reason?>" <?php if($row->ID_Reason==$driver->InactiveReason){ echo 'selected';}?>><?php echo $row->ReasonText;?></option>
<?php endforeach;?>
</select>
</div>
</div>
</div>
<!-- <div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Latitude <span style="color:red;"><?php echo form_error('latitude');?></label>
<input type="text" class="form-control" value="<?php echo $driver->Latitude;?>"  name="latitude" placeholder="Latitude" >
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	 Longitude<span style="color:red;"><?php echo form_error('longitude');?></label>
<input type="text" class="form-control"  name="longitude" value="<?php echo $driver->Longitude;?>" placeholder="Longitude" >
</div>
</div>
</div> -->
<!-- <div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Expiry of CRB Certification <span style="color:red;"><?php echo form_error('crb');?></label>
<input type="text" class="form-control datetimepicker"  value="<?php echo $driver->CRBExpiry;?>" name="crb" placeholder="Expiry or CRB Certification" >
</div> -->
<div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Driving licence <span style="color:red;"><?php echo form_error('drl');?></label>
<input type="text" class="form-control" value="<?php echo $driver->DrivingLicenceNo;?>"  name="drl" placeholder="Driving licence" >
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1"> Expiry Date<span style="color:red;"><?php echo form_error('lexp');?></label>
<input type="datetime" class="form-control datetimepicker" value="<?php echo $driver->DrivingLicenceExpiry;?>"  name="lexp" placeholder="License Expiry Date" >
</div>
</div>
</div>
<div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Badge No <span style="color:red;"><?php echo form_error('BadgeNo');?></label>
<input type="text" class="form-control" value="<?php echo $driver->BadgeNo;?>"  name="BadgeNo" placeholder="Badge No" >
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1"> Expiry Date<span style="color:red;"><?php echo form_error('badgexp');?></label>
<input type="datetime" class="form-control datetimepicker" value="<?php echo $driver->BadgeExpiry;?>"  name="badgexp" placeholder="Badge Expiry Date" >
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Bank account number <span style="color:red;"><?php echo form_error('AccountNumber');?></label>
<input type="text" class="form-control"  value="<?php echo $driver->BankAccountNo;?>" name="AccountNumber" placeholder="Bank account number" >
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	DBS check <span style="color:red;"><?php echo form_error('CRBExpiry');?></label><br>
<!-- <input type="checkbox" id="crb" value="<?php echo $driver->CRBExpiry;?>" name="CRBExpiry"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" > -->
<input type="checkbox" id="crb" value="<?php echo $driver->CRBExpiry;?>" name="CRBExpiry"  data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="info" >
</div>
</div>
</div>

<div class="col-xs-12 col-md-6">
<div class="row">
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Payment Expiry <span style="color:red;"><?php echo form_error('PaymentExpiry');?></label>
<input type="dattime" class="form-control datetimepicker" value="<?php echo $driver->PaymentExpiry;?>"  name="PaymentExpiry"  placeholder="Payment Expiry" id="form-field-1">
</div>
<div class="form-group col-xs-12 col-md-6">
<label  for="form-field-1">	Barred Expiry <span style="color:red;"><?php echo form_error('BarredExpiry');?></label>
<input type="dattime" class="form-control datetimepicker" value="<?php echo $driver->BarredExpiry;?>" name="BarredExpiry" placeholder="Barred Expiry" id="form-field-1">
</div>
</div>
</div>
<br>
<br>

</div>
</fieldset>
<div class="col-xs-12 col-md-12">
<fieldset>
<legend class="color">Images</legend>
<div class="row">
<div class="form-group col-xs-12 col-md-6">
					<input type="file" name="upload_file1" id="upload_file1"/>
                    <div id="moreImageUpload"></div>
                    <div id="moreImageUploadLink" style="display:none;margin-left: 10px;">
                        <a href="javascript:void(0);" id="attachMore">Add More</a>
                    </div>
</div>
</div>

<div class="form-group col-xs-12 col-sm-6">

<?php if(!empty($images)) :  foreach($images as $img) : ?>
	<img width="100px" src="<?php echo base_url();?>assets/images/driverimage/<?php echo $img->Image;?>">
	<a href="<?php echo base_url() ?>drivers/deleteimage/<?php echo $img->ID_Image; ?>/<?php echo $this->uri->segment(3);?>"> X </a>
<?php endforeach; endif; ?>
</div>

</fieldset>
</div>




</div>

<div class="col-xs-12">

<div class=" col-md-3 row pull-right text-right">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;
<a href="<?php echo base_url();?>drivers" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>

</div>


</div>

</form>
</div>



<script>

 $(document).ready(function () {
        $("input[id^='upload_file']").each(function () {
            var id = parseInt(this.id.replace("upload_file", ""));
            $("#upload_file" + id).change(function () {
                if ($("#upload_file" + id).val() !== "") {
                    $("#moreImageUploadLink").show();
                }
            });
        });
    });

 $(document).ready(function () {
        var upload_number = 2;
        $('#attachMore').click(function () {
            //add more file
            var moreUploadTag = '';
            moreUploadTag += '<input type="file" id="upload_file' + upload_number + '" name="upload_file' + upload_number + '"/>';
            moreUploadTag += '&nbsp;<a href="javascript:del_file(' + upload_number + ')" style="cursor:pointer;" onclick="return confirm(\"Are you really want to delete ?\")">Delete </a></div>';
            $('<dl id="delete_file' + upload_number + '">' + moreUploadTag + '</dl>').fadeIn('slow').appendTo('#moreImageUpload');
            upload_number++;
        });
    });

    function del_file(eleId) {
        var ele = document.getElementById("delete_file" + eleId);
        ele.parentNode.removeChild(ele);
    }




</script>
