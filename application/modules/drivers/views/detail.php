
            <div class=" col-xs-12 col-sm-12">
              <fieldset>
                <legend class="color">Driver Detail</legend>
                <table id="simple-table" class="table table-bordered table-hover">
                  <tr>
                    <th width="11%">Name</th>
                  <td><?php echo ucfirst($driver->FirstName.' '.$driver->LastName) ;?></td>
                  <th  width="11%">Image</th>
                    <td>
                      <?php foreach($driverimages as $row):?>
                      <img width="50" height="50" src="<?php echo base_url();?>assets/images/driverimage/<?php echo $row->Image;?>">
                    <?php endforeach;?>
                    </td>
                  </tr>
                  <tr>
                  <th>Gender</th>
                  <td><?php echo $driver->Gender;?></td>
                   <th>Company</th>
                    <td><?php echo $driver->company;?></td>
                </tr>


                 <tr>
                  <th>Email</th>
                  <td><?php echo $driver->Email;?></td>
                  <th>Country</th>
                    <td><?php echo $driver->country;?></td>
                </tr>

                <tr>
                  <th>Telephone1</th>
                  <td><?php echo $driver->Telephone1;?></td>
                  <th>Start Date</th>
                  <td><?php echo $driver->StartDate;?></td>
                </tr>
                <tr>
                  <th>Telephone2</th>
                  <td><?php echo $driver->Telephone2;?></td>
                  <th>  Finish Date</th>
                  <td><?php echo $driver->FinishDate;?></td>
                </tr>
                <tr>
                  <th>Address1</th>
                  <td><?php echo $driver->Address1;?></td>
                  <th>Status</th>
                  <td><?php if($driver->Status==0){
            echo '<span class="label status label-sm label-warning">Off</span>';
          }
          else if($driver->Status==1)
          {
          echo '<span class="label status label-sm label-success">Scrub</span>';
          }else if($driver->Status==2)
          {
          echo '<span class="label status label-sm label-success">Clear</span>';
          }else if($driver->Status==3)
          {
          echo '<span class="label status label-sm label-success">Bidding</span>';
          }
          else if($driver->Status==4)
          {
          echo '<span class="label status label-sm label-success">OnRoute</span>';
          }else if($driver->Status==5)
          {
          echo '<span class="label status label-sm label-success">Soon to Arrive</span>';
          }else if($driver->Status==6)
          {
          echo '<span class="label status label-sm label-success">Arrived</span>';
          }else if($driver->Status==7)
          {
          echo '<span class="label status label-sm label-success">POB</span>';
          }else if($driver->Status==8)
          {
          echo '<span class="label status label-sm label-success">Confirm</span>';
          }else if($driver->Status==9)
          {
          echo '<span class="label status label-sm label-success">Busy</span>';
          }else if($driver->Status==10)
          {
          echo '<span class="label status label-sm label-success">Undefined</span>';
          }
          else
          {
          echo '<span class="label status label-sm label-success">Driver Paid</span>';
          }?></td>
                </tr>
                <tr>
                  <th>Address2</th>
                  <td><?php echo $driver->Address2;?></td>
                  <th>Active</th>
                  <td><?php
                  if($driver->Active==0){
            echo '<span class="label status label-sm label-warning">Inactive</span>';
          }
          else if($driver->Active==1)
          {
          echo '<span class="label status label-sm label-success">Active</span>';
          }else
          {
          echo '<span class="label status label-sm label-success">Pending</span>';
          }
              ?>
                </td>
                </tr>
                <tr>
                  <th>Address3</th>
                  <td><?php echo $driver->Address3;?></td>
                  <th>Latitude</th>
                  <td><?php echo $driver->Latitude;?></td>
                </tr>
                <tr>
                  <th>Nationality</th>
                  <td><?php echo $driver->Nationality;?></td>
                  <th>Longitude</th>
                  <td><?php echo $driver->Longitude;?></td>
                </tr>
                <tr>
                  <th>Driving License</th>
                  <td><?php echo $driver->DrivingLicenceNo;?></td>
                  <th>Expiry Date</th>
                  <td><?php echo $driver->DrivingLicenceExpiry;?></td>
                </tr>
                <tr>
                  <th>Badge No</th>
                  <td><?php echo $driver->BadgeNo;?></td>
                  <th>Expiry Date</th>
                  <td><?php echo $driver->BadgeExpiry;?></td>
                </tr>
                 <tr>
                  <th>Payment Expiry</th>
                  <td><?php echo $driver->PaymentExpiry;?></td>
                  <th>Barred Expiry</th>
                  <td><?php echo $driver->BarredExpiry;?></td>
                </tr>
                <tr>
                  <th>Expiry of CRB </th>
                  <td><?php echo $driver->CRBExpiry;?></td>
                </tr>
                </table>
                    </fieldset>
              </div>
              <fieldset >
              <?php // foreach($cars as $car):?>
              <div class="col-xs-12 sm">
              <div class="col-xs-12 shadow">
            <div class="row">
              <legend class="color bg">Car Detail</legend>
              <div class='col-xs-12 col-sm-4'>
					  <?php if(!empty($car->main_image->Image)) : ?>
                      <img style="width:100%; height:286px;" src="<?php echo base_url();?>assets/images/carimage/<?php echo $car->main_image->Image;?>">
					  <?php endif; ?>
             <div class="col-xs-12 row">
              <?php if($car->images):foreach($car->images as $row):?>
                      <?php if($row->Image==$car->main_image->Image):?>
                      <?php else:?>
                      <img width="60" class="xs" height="60" src="<?php echo base_url();?>assets/images/carimage/<?php echo $row->Image;?>">
                      <?php endif;?>
               <?php  endforeach;endif;?>
          </div>
              </div>
              <div class='col-xs-12 col-sm-8'>
                <table  class="table table-bordered table-hover">
                  <tr>
                  <th>Make</th>
                  <td><?php echo $car->Make;?></td>
                  <th>Model</th>
                  <td><?php echo $car->Model;?></td>
                </tr>
                 <tr>
                  <th>Colour</th>
                  <td><?php echo $car->Colour;?></td>
                  <th>Registration Plate</th>
                  <td><?php echo $car->RegistrationPlate;?></td>
                </tr>
                <tr>
                  <th>PIN</th>
                  <td><?php echo $car->PIN;?></td>
                  <th>Registration Date</th>
                  <td><?php echo $car->RegistrationDate;?></td>
                </tr>
                <tr>
                  <th>Passenger Seats</th>
                  <td><?php echo $car->PassengerSeats;?></td>
                  <th>Category</th>
                  <td><?php echo $car->Category;?></td>
                </tr> <tr>
                  <th>Operating Plate</th>
                  <td><?php echo $car->OperatingPlate;?></td>
                  <th>Expiry Date</th>
                  <td><?php echo $car->OperatingPlateExpDate;?></td>
                </tr>  <tr>
                  <th>MOT Expiry Date</th>
                  <td><?php echo $car->MOTExpDate;?></td>
                  <th>Tax Expiry Date</th>
                  <td><?php echo $car->TaxExpDate;?></td>
                </tr>  <tr>
                  <th>Insurance Expiry Date</th>
                  <td><?php echo $car->InsuranceExpDate;?></td>
                  <th>Insurance Type</th>
                  <td><?php
                  if ($car->InsuranceType==0) {
                    echo 'Unknown';
                  }elseif ($car->InsuranceType==1) {
                    echo 'Third Party';
                  }elseif ($car->InsuranceType==2) {
                    echo 'Third Party Fire & Theft';
                  }elseif ($car->InsuranceType==3) {
                    echo 'Fully Comprehensive';
                  }else{
                    echo 'Fleet Insurance';
                  }
                  ?></td>
                </tr> <tr>
                  <th>Insurance Provider</th>
                  <td><?php echo $car->InsuranceProvider;?></td>
                  <!-- <th>Company</th>
                  <td><?php echo $car->company;?></td> -->
                </tr>
                </table>

              </div>
              </div>
              </div>
              </div>

              <div class="col-xs-12 sm">
              <div class="col-xs-12 shadow">
            <div class="row">
              <legend class="color bg">Driver Billing</legend>
              <div class='col-xs-12 col-sm-4'>
                <?php if($billing):foreach($billing as $car):?>
                <table  class="table table-bordered table-hover">
                  <tr>
                  <th>Amount</th>
                  <td><?php echo $car->Amount;?></td>
                </tr>
                 <tr>
                  <th>Tax</th>
                  <td><?php echo $car->Tax;?></td>
                </tr>
                 <tr>
                  <th>Payment Type</th>
                  <td><?php echo $car->PaymentType;?></td>
                </tr>
                <tr>
                  <th>CardNumber</th>
                  <td><?php echo $car->CardNumber;?></td>
                </tr>
                <tr>
                  <th>Job</th>
                  <td><?php echo $car->ID_Job;?></td>

                </tr> <tr>
                  <th>Invoice No</th>
                  <td><?php echo $car->InvoiceNo;?></td>
                </tr>  <tr>
                  <th>Invoice Date</th>
                  <td><?php echo $car->InvoiceDate;?></td>
                </tr>
                </table>
              <?php endforeach;  endif;?>

              </div>
              </div>
              </div>
              </div>
              <div class="col-xs-12 sm">
              <div class="col-xs-12 shadow">
            <div class="row">
              <legend class="color bg">Driver Payment</legend>
              <div class='col-xs-12 col-sm-4'>
                <?php if($payment):foreach($payment as $car):?>
                <table  class="table table-bordered table-hover">
                  <tr>
                  <th>Amount</th>
                  <td><?php echo $car->Amount;?></td>
                </tr>
                <tr>
                  <th>Payment Date</th>
                  <td><?php echo $car->PaymentDateTime;?></td>
                </tr>
              <tr>
                  <th>Invoice No.</th>
                  <td><?php echo $car->InvoiceNo;?></td>
                </tr>
                </table>
              <?php endforeach;  endif;?>

              </div>
              </div>
              </div>
              </div>
              </fieldset>
