
<style type="text/css">

.status{
	border-radius: 3px;
    font-size: 15px;
    height: auto;
    padding: 6px 9px;
}
</style>
<?php if ($this->session->userdata('item')):?>
	<div class="alert alert-block alert-success col-xs-11">
	<button data-dismiss="alert" class="close" type="button">
		<i class="ace-icon fa fa-times"></i>
	</button>

	<i class="ace-icon fa fa-check green"></i>
	<?php echo $this->session->flashdata('item');?>
</div>
<?php endif;?>
						<div class="page-header col-xs-12">
							<h1>
								<?php echo $title;?>
								<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		<?php echo $title;?>
	</small>
							</h1>

						</div><!-- /.page-header -->
						<div class="row">
						<div class="col-xs-12">
							<div class="row">
									<div class="col-xs-12">
								<?php /*		<a style="margin:5px 6px;" href="<?php echo base_url();?>drivers/add" class="btn btn btn-primary pull-right"><i class="ace-icon glyphicon glyphicon-plus"></i> Add</a> */ ?>
										<table class="table datatable table-bordered table-hover" id="simple-table">
											<thead>
												<tr>
													<th class="detail-col">ID</th>
													<th>Name</th>
													<th>Images</th>
													<th>Email</th>
													<th>Phone Number</th>
													<th>Bank Account </th>
													<th>licence Number</th>
													<th>Expiry Date</th>
													<th>Status</th>
													<th>Options</th>
								</tr>
											</thead>

											<tbody>
												<?php foreach($drivers as $row):?>
												<tr>
													<td class=" bigger-120 center">

												<?php echo $row->ID_Driver;?>

													</td>
												<td class=" bigger-120 center">

												<?php echo ucfirst($row->FirstName); ?> <?php echo ucfirst($row->LastName); ?>

													</td>
													<td>

												<?php foreach($row->images as $img): ?>
												<img width="50" height="50" src="<?php echo base_url();?>assets/images/driverimage/<?php echo $img->Image;?>">
												<?php endforeach;?>
													</td>
													<td class=" bigger-120 center">

												<?php echo $row->Email?>

													</td>
													<td class="bigger-120">
												<?php echo $row->Telephone1?>
													</td>

													<td class="bigger-120">
												<?php echo $row->BankAccountNo?>
													</td>
													<td class="bigger-120">
												<?php echo $row->DrivingLicenceNo?>
													</td>

													<td class="bigger-120">
												<?php echo $row->DrivingLicenceExpiry?>
													</td>
													<td class="bigger-120">
												<?php

					if($row->Status==0){
						echo '<span class="label status label-sm label-warning">Off</span>';
					}
					else if($row->Status==1)
					{
					echo '<span class="label status label-sm label-success">Scrub</span>';
					}else if($row->Status==2)
					{
					echo '<span class="label status label-sm label-success">Clear</span>';
					}else if($row->Status==3)
					{
					echo '<span class="label status label-sm label-success">Bidding</span>';
					}
					else if($row->Status==4)
					{
					echo '<span class="label status label-sm label-success">OnRoute</span>';
					}else if($row->Status==5)
					{
					echo '<span class="label status label-sm label-success">Soon to Arrive</span>';
					}else if($row->Status==6)
					{
					echo '<span class="label status label-sm label-success">Arrived</span>';
					}else if($row->Status==7)
					{
					echo '<span class="label status label-sm label-success">POB</span>';
					}else if($row->Status==8)
					{
					echo '<span class="label status label-sm label-success">Confirm</span>';
					}else if($row->Status==9)
					{
					echo '<span class="label status label-sm label-success">Busy</span>';
					}else if($row->Status==10)
					{
					echo '<span class="label status label-sm label-success">Undefined</span>';
					}
					else
					{
					echo '<span class="label status label-sm label-success">Driver Paid</span>';
					}

					?>
													</td>
													<td>
														<div class=" btn-group action-buttons">

															<a class="green "  href="<?php echo base_url();?>drivers/add/<?php echo $row->ID_Driver;?>">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</a>
															<a class="green "  href="<?php echo base_url();?>drivers/driver_detail/<?php echo $row->ID_Driver;?>">
																<i class="ace-icon fa fa-eye bigger-120"></i>
															</a>

															<a class="red confirm" href="<?php echo base_url();?>drivers/delete/<?php echo $row->ID_Driver;?>">
																<i class="ace-icon fa fa-trash-o bigger-120"></i>
															</a>


														</div>


													</td>
												</tr>
											<?php endforeach;?>
											</tbody>
										</table>
									</div><!-- /.span -->
								</div>

						</div>

						</div>
