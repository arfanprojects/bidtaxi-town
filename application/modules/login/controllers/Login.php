<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Controller
 * @author      M Arfan
 * @copyright   (c) 2014 CMS
 */
class Login extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('drivers/drivers_m');
        $this->load->model('cars/cars_m');
        $this->load->library('form_validation');
        $this->db1 = $this->load->database('master', true);
    }

    public function index() {

        if ($this->session->userdata('is_loggedin')) {
            redirect('dashboard');
        }
        // Apply login rules 
        $rules = $this->admin_model->rules_login;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            // if validation true validate username and password
            if ($this->admin_model->login()) {
                $this->admin_model->update($this->session->userdata('user_id'),array('status' =>2));
                $this->admin_model->update($this->session->userdata('user_id'),array('LoggedOn' =>date('Y-m-d h:i:s')));
                redirect("dashboard");
            } else {
                // login failed 
                $this->session->set_flashdata('error', 'Invalid Username OR Password');
                redirect('login');
            }
        }
        $this->db->select("ID_Company");
        $query = $this->db->get("administrator");
        $this->db->distinct();
        $result = $query->result();
        $ids = array();
        foreach ($result as $d) {
            $ids[] = $d->ID_Company;
        }
        $data_r = implode(",", $ids);

        $data['companies'] = $this->db1->where_in("ID_Company", explode(",", $data_r))->get('companies')->result();
        $this->load->view('login/login', $data);
    }

    public function logout() {
        $this->admin_model->logout();
        $this->admin_model->update($this->session->userdata('user_id'), array('status' => 1));
        $this->admin_model->update($this->session->userdata('user_id'),array('LoggedOff' =>date('Y-m-d h:i:s')));
        redirect('login');
    }

    public function register() {
        $rules = $this->admin_model->rules_register;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            // if validation true validate username and password
            $data = array(
                'FirstName' => $this->input->post('firstname'),
                'LastName' => $this->input->post('lastname'),
                'AccessLevel' => 3,
                'ID_Company' => $this->input->post('company'),
                'UserName' => $this->input->post('username'),
                'Email' => $this->input->post('email'),
                'Password' => md5($this->input->post('password'))
            );

            $id_driver = $this->admin_model->insert($data);
            $driver = array(
                'FirstName' => $data['FirstName'],
                'LastName' => $data['LastName'],
                'Email' => $data['Email'],
                'ID_Company' => $data['ID_Company'],
                'ID_Driver' => $id_driver
            );
            $this->drivers_m->insert($driver);
            if ($id_driver) {
                $car_id = $this->cars_m->insert(array('ID_Driver' => $id_driver, 'ID_Company' => $data['ID_Company']));
            }
            $this->session->set_flashdata('error', 'Successfully Registered.Now you can login to continue....');
            redirect('login');
        } else {
            $this->session->set_flashdata('error', validation_errors());
            $this->load->view('login/login');
        }
    }

}
