<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class admin_model extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_Administrator';
    protected $_table = 'administrator';
    public $rules_login = array(
        array(
            'field' => 'UserName',
            'label' => 'UserName',
            'rules' => 'required|trim'),
        array(
            'field' => 'Password',
            'label' => 'Password',
            'rules' => 'required|trim')
    );
    public $rules_register = array(
        array(
            'field' => 'username',
            'label' => 'UserName',
            'rules' => 'required|trim'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|trim'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|trim|valid_email'
        )
    );

    public function __construct() {
        parent::__construct();
    }

    /**
     * Admin login
     * @return boolean
     */
    public function login() {

        $user = $this->get_by(array(
            'UserName' => $this->input->post('UserName'),
            'Password' => md5($this->input->post('Password')),
        ));
        if (count($user)) {
            // Log in user and store in session
            $data = array(
                'UserName' => $user->UserName,
                'FirstName' => $user->FirstName,
                'LastName' => $user->LastName,
                'Email' => $user->Email,
                'Status' => $user->Status,
                'user_id' => $user->ID_Administrator,
                'AccessLevel' => $user->AccessLevel,
                'ID_Company' => $user->ID_Company,
                'is_loggedin' => 1,
            );
            
            $this->session->set_userdata($data);
            return true;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
    }

    public function loggedin() {
        return $this->session->userdata('loggedin');
    }

}
