<?php if ($this->session->userdata('item')):?>
	<div class="alert alert-block alert-success col-xs-11">
	<button data-dismiss="alert" class="close" type="button">
		<i class="ace-icon fa fa-times"></i>
	</button>

	<i class="ace-icon fa fa-check green"></i>
	<?php echo $this->session->flashdata('item');?>
</div>
<?php endif;?>
						<div class="page-header col-xs-12">
							<h1>
								<?php echo $title;?>
								<small>
								<i class="ace-icon fa fa-angle-double-right"></i>
								<?php echo $title;?>
							</small>
							</h1>

						</div><!-- /.page-header -->
						<div class="row">
						<div class="col-xs-12">
							<div class="row">
									<div class="col-xs-12">
										<a style="margin:5px 6px;" href="<?php echo base_url();?>driver_job/add" class="btn btn btn-primary pull-right"><i class="ace-icon glyphicon glyphicon-plus"></i> Add</a>
										<table class="table  table-bordered table-hover" id="simple-table">
											<thead>
												<tr>
													<th class="detail-col">ID</th>
													<th>Job</th>
													<th>Driver</th>
													<th>Booking Date</th>
													<th>Options</th>
												</tr>
											</thead>

											<tbody>
												<?php foreach($driver_job as $row):?>
												<tr>
												
													<td class=" bigger-120 center">
														
												<?php echo $row->ID_Driver_Job;?>
															
													</td>
													<td class="bigger-120">
												<?php echo $row->Job;?>
													</td>
													<td class="bigger-120">
												<?php echo $row->FirstName.' '.$row->LastName;?>
													</td>
													<td class="bigger-120">
												<?php echo $row->DateTimeBooked?>
													</td>
													
													<td>
														<div class=" btn-group action-buttons">

															<!-- <a class="green "  href="<?php echo base_url();?>driver_job/add/<?php echo $row->ID_Driver_Job;?>">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</a> -->

															<a class="red confirm" href="<?php echo base_url();?>driver_job/delete/<?php echo $row->ID_Driver_Job;?>">
																<i class="ace-icon fa fa-trash-o bigger-120"></i>
															</a>

															
														</div>

														
													</td>
												</tr>
											<?php endforeach;?>
											</tbody>
										</table>
									</div><!-- /.span -->
								</div>

						</div>

						</div>