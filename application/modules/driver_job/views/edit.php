<div class="page-header">
	<h1>
		<?php echo $title;?>
		<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		<?php echo $title;?>
	</small>
	</h1>

</div><!-- /.page-header -->
<div class="col-xs-12 col-sm-6">
<?php echo form_open('driver_job/add'.$driver_job->ID_Driver_Job);?>
<div class="row">
<div class="form-group col-sm-6">
<label>	Job <span style="color:red;"><?php echo form_error('ID_Job');?></label>
<select name="ID_Job" class="form-control">
<option value="">Select Job</option>
<?php foreach($jobs as $row):?>
<option value="<?php echo $row->ID_Job;?>" <?php if($row->ID_Job==$driver_job->ID_Job){ echo 'selected';}?>><?php echo $row->ID_Job?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-sm-6">
<label>	Driver <span style="color:red;"><?php echo form_error('ID_Driver');?></label>
<select name="ID_Driver" class="form-control">
<option value="">Select Driver</option>
<?php foreach($drivers as $row):?>
<option value="<?php echo $row->ID_Driver;?>" <?php if($row->ID_Driver==$driver_job->ID_Driver){ echo 'selected';}?>><?php echo $row->FirstName.' '.$row->LastName;?></option>
<?php endforeach;?>
</select>
</div>
</div>
<div class="form-group">
<label>	Booking Date <span style="color:red;"><?php echo form_error('DateTimeBooked');?></label>
<input type="text" class="form-control datetimepicker" required name="DateTimeBooked" placeholder="Booking Date">
</div>
<div class="col-xs-12 row">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>driver_job" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>



</div></form>
</div>

