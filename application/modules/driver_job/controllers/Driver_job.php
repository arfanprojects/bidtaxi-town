<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Driver_job extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Driver_job_m');
        $this->load->model('drivers/drivers_m');
        $this->load->model('jobs/jobs_m');
    }

    public function index() {
        $data['view'] = "driver_job/home";
        $data['title'] = "Driver Jobs";
        $data['driver_job'] = $this->Driver_job_m->get_jobs();
        $this->load->view('theme/layout', $data);
    }
    
    public function add($id='') {
    	 $this->load->library("form_validation");
    	 $rules = $this->Driver_job_m->rules_add;
    	 $this->form_validation->set_rules($rules);
         if ($this->form_validation->run() == TRUE) {
        $data=array(
            'ID_Job' =>$this->input->post('ID_Job'),
            'ID_Driver' =>$this->input->post('ID_Driver'),
            'DateTimeBooked' =>$this->input->post('DateTimeBooked')
        	);
        if($id){
        $this->Driver_job_m->update($id,$data);
        $this->session->set_flashdata('item','Successfully Updated!');
        }
        else
        {
        $this->Driver_job_m->insert($data);
        $this->session->set_flashdata('item','Successfully Added!');
        }
        
        redirect('driver_job');
	    }
	    else
	    {
        $data['jobs'] = $this->jobs_m->get_all();
        $data['drivers'] = $this->drivers_m->get_many_by("ID_Company" , $this->session->userdata("ID_Company"));
	    if ($id) {
        $data['driver_job'] = $this->Driver_job_m->get($id);
        $data['view'] = "driver_job/edit";
        $data['title'] = "Edit Job";
        }
        else
        {
        $data['view'] = "driver_job/add";
        $data['title'] = "Add Job";  
        }
        $this->load->view('theme/layout', $data);
	    }
    

}
public function delete($id=''){
    	$this->Driver_job_m->delete($id);
    	redirect('driver_job');
    }

}