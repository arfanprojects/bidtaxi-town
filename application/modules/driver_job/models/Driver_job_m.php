<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Driver_job_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_Driver_Job';
    protected $_table = 'driverjob';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'ID_Job',
            'label' => 'Job',
            'rules' => 'required'
        ),array(
            'field' => 'ID_Driver',
            'label' => 'Driver',
            'rules' => 'required'
        ),array(
            'field' => 'DateTimeBooked',
            'label' => 'Booking Date',
            'rules' => 'required'
        )
    );
    public function get_jobs(){
        $query=$this->db->select('*')->from('driverjob as dj')
                        ->join('job','job.ID_Job=dj.ID_Job','left')
                        ->join('driver as d','d.ID_Driver=dj.ID_Driver','left')
                        ->get();
                        return $query->result();
    }
}
