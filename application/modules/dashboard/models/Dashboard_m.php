<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Dashboard_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'id';
    protected $_table = 'dashboard';
    public function __construct() {
        parent::__construct();
    }
	
	    public function get_customer_between_date($start_date = "", $end_date = "") {
        if ($start_date and $end_date) {
            $this->db->where('created_at >=', $start_date);
            $this->db->where('created_at <=', $end_date);
        }
        if ($this->session->userdata('group_id') != 2211) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }
        $query = $this->db->get('customers');
        return $query->result();
    }
	public function get_payment_between_date($start_date = "", $end_date = "") {
        $this->db->select(" SUM(price) as total_price");
        $this->db->from('order_payment');
        if ($start_date and $end_date) {
            $this->db->where('created_date >=', $start_date);
            $this->db->where('created_date <=', $end_date);
        } 
        if($this->session->userdata('group_id') != 2211) { 
            $this->db->where("user_id" , $this->session->userdata('user_id'));
        }
        $query = $this->db->get();
        $row =  $query->row();
		 $this->db->last_query();
        return $row->total_price;
    }
	
	function get_commision_period() {
         $this->db->where("current" , 1);
         $this->db->where("user_id" , $this->session->userdata("user_id"));
         $query = $this->db->get('commission_periods');
         return $query->row();
    }
	
	
	public function get_user_data($start_date = '', $end_date = '') {
	
        if ($this->session->userdata('group_id') == "3002") {
            $this->db->where('user_id', $this->session->userdata('user_id'));
        }
        if (!empty($start_date) || !empty($end_date)) {
          
            $this->db->where('created_at >=', $start_date);
            $this->db->where('created_at <=', $end_date);
        }
        $this->db->where("order_status != ", "Paid");
       
        $query = $this->db->get("order");
        $order_invoice_payment = 0;
        $order_payment = 0;
		$total_orders = 0;
        foreach ($query->result() as $order) {
			$total_orders = $total_orders + 1;
             $order_invoice_payment = $order_invoice_payment + $this->get_invoice_payment_by_order($start_date, $end_date , $order->id);
             $order_payment = $order_payment + $this->get_payment_by_order($start_date, $end_date ,$order->id);
        }
        $final_ressult = array();
        $final_ressult['invoice_payment'] = $order_invoice_payment;
        $final_ressult['order_payment'] = $order_payment;
        $final_ressult['total_orders'] = $total_orders;
        return $final_ressult;
    }
	
	
	public function get_invoice_payment_by_order($start_date = '', $end_date = '' , $order_id) {
        $this->db->select("SUM(price) as total_price");
        $this->db->from("order_invoice");
		if (!empty($start_date) || !empty($end_date)) {
          
            $this->db->where('created_date >=', $start_date);
            $this->db->where('created_date <=', $end_date);
        }
		 
        $this->db->where("order_id", $order_id);
        $query = $this->db->get();
        $row = $query->row();
        if ($row) {
            return $row->total_price;
        } else {
            return 0;
        }
    }
	
	public function get_payment_by_order($start_date = '', $end_date = '' ,$order_id) {
        $this->db->select("SUM(price) as total_price");
        $this->db->from("order_payment");
		if (!empty($start_date) || !empty($end_date)) {
          
            $this->db->where('created_date >=', $start_date);
            $this->db->where('created_date <=', $end_date);
        }
        $this->db->where("order_id" , $order_id);
        $query = $this->db->get();
        $row = $query->row();
		
        if($row) { 
            return $row->total_price;
        } else { 
            return 0;
        }
    }

 




}
