<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Carimage_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_Image';
    protected $_table = 'carimage';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'ImageType',
            'label' => 'Image Type',
            'rules' => 'required|trim'
        ),array(
            'field' => 'ID_Car',
            'label' => 'Car',
            'rules' => 'required|trim'
         )
           // ,array(
        //     'field' => 'ImageSequence',
        //     'label' => 'Image Sequence',
        //     'rules' => 'required|trim'
        // )
    );
    public function get_carimage(){
        $query=$this->db->select('*')->from('carimage as ci')
                        ->join('car as c','c.ID_Car=ci.ID_Car','left')
                        ->get();
                        return $query->result();
    }
}
