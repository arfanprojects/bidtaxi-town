<div class="page-header">
	<h1>
		<?php echo $title;?>
		<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		<?php echo $title;?>
	</small>
	</h1>

</div><!-- /.page-header -->
<div class="col-xs-12 col-sm-6">
<?php echo form_open_multipart('carimage/add/'.$carimage->ID_Image);?>
<div class="row">
<div class="form-group col-sm-6">
<label>	Car <span style="color:red;"><?php echo form_error('ID_Car');?></label>
<select name="ID_Car" class="form-control">
<option value="">Select Car</option>
<?php foreach($cars as $row):?>
<option value="<?php echo $row->ID_Car;?>" <?php if($row->ID_Car==$carimage->ID_Car){ echo 'selected';}?>><?php echo $row->ID_Car;?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-sm-6">
<label>Image Type <span style="color:red;"><?php echo form_error('ImageType');?></label>
<select name="ImageType" class="form-control">
<option value="">Select Type</option>
<option value="1" <?php if($carimage->ImageType==1){ echo 'selected';}?>>Registration Document</option>
<option value="2" <?php if($carimage->ImageType==2){ echo 'selected';}?>>Operating Plate Document</option>
<option value="3" <?php if($carimage->ImageType==3){ echo 'selected';}?>>MOT Certificate</option>
<option value="4" <?php if($carimage->ImageType==4){ echo 'selected';}?>>Insurance Certificate</option>
</select>
</div>
<!-- <div class="form-group col-sm-12">
<label>	Image Sequence <span style="color:red;"><?php echo form_error('ImageSequence');?></label>
<select name="ImageSequence" class="form-control" multiple>
<option value="">Select Sequence</option>
<option value="1" <?php if($carimage->ImageSequence==1){ echo 'selected';}?>>Registration Document</option>
<option value="2" <?php if($carimage->ImageSequence==2){ echo 'selected';}?>>Operating Plate Document</option>
<option value="3" <?php if($carimage->ImageSequence==3){ echo 'selected';}?>>MOT Certificate</option>
<option value="4" <?php if($carimage->ImageSequence==4){ echo 'selected';}?>>Insurance Certificate</option>
</select>
</div> -->
</div>
<div class="form-group">
<label>	Image <span style="color:red;"><?php echo form_error('Image');?></label>
<input type="file" name="Image">
<?php if($carimage->Image):?>
<img style="margin-top:7px;" src="<?php echo base_url();?>assets/images/carimage/<?php echo $carimage->Image;?>" width="100">
<?php endif;?>
<input type="hidden"  name="image" value="<?php echo $carimage->Image;?>">
</div>
<div class="col-xs-12 row">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>carimage" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>


</div>
</form>
</div>
















