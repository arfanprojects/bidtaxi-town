<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Carimage extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('carimage_m');
        $this->load->model('cars/cars_m');
    }

    public function index() {
        $data['view'] = "carimage/home";
        $data['title'] = "Car Images";
        $data['carimage'] = $this->carimage_m->get_carimage();
        $this->load->view('theme/layout', $data);
    }
    
    public function add($id='') {
    	 $this->load->library("form_validation");
    	 $rules = $this->carimage_m->rules_add;
    	 $this->form_validation->set_rules($rules);
         if ($this->form_validation->run() == TRUE) {
        $data=array(
            'ImageType' =>$this->input->post('ImageType'),
            'ID_Car' =>$this->input->post('ID_Car'),
            'ImageSequence' =>$this->input->post('ImageSequence')
        	);

        if (file_exists($_FILES['Image']['tmp_name'])) {
         $img_response = $this->add_image();
         $data['Image'] = $img_response['name'].'_o.'.$img_response['ext'];
        }
        if($id){
        if (file_exists($_FILES['Image']['tmp_name'])) {
        $image = $this->input->post('image');
        unlink(FCPATH.'/assets/images/carimage/'.$image);
        }
        $this->carimage_m->update($id,$data);
        $this->session->set_flashdata('item','Successfully Updated!');
        }
        else
        {
        $this->carimage_m->insert($data);
        $this->session->set_flashdata('item','Successfully Added!');
        }
        
        redirect('carimage');
	    }
	    else
	    {
        $data['cars'] = $this->cars_m->get_all();
	    if ($id) {
        $data['carimage'] = $this->carimage_m->get($id);
        $data['view'] = "carimage/edit";
        $data['title'] = "Edit Car Image";
        }
        else
        {
        $data['view'] = "carimage/add";
        $data['title'] = "Add Car Image";  
        }
        $this->load->view('theme/layout', $data);
	    }
    

}
public function add_image() {
        $time = time() . rand();
        $post_image = array(
            'file' => $_FILES["Image"],
            'new_name' => $time . '_o',
            'dst_path' => FCPATH.'/assets/images/carimage/'
        );
        $post_image_response = upload_original($post_image);
        return array('name' => $time, 'ext' => $post_image_response['file_ext']);
    }
public function delete($id='') {
        $detail = $this->carimage_m->get($id);
        // unlink(FCPATH.'/assets/images/carimage/'.$detail->image."_s.".$detail->flight_image_ext);
        unlink(FCPATH.'/assets/images/carimage/'.$detail->image);
        $this->carimage_m->delete($id);
        redirect('carimage');
    }


}