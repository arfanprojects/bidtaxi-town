<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('administrator_m');
        $this->load->library('pagination');
    }

    public function index() {
        //pagination settings
        $config['base_url'] = site_url('administrator/index');
        $config['total_rows'] = $this->db->count_all('administrator');
        $config['per_page'] = 10;
        $offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '&laquo; First';
        $config['last_link'] = 'Last &raquo;';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['pagination'] = $this->pagination->create_links();
        $data['view'] = "administrator/home";
        $data['title'] = "Administrator";
        $data['administrator'] = $this->administrator_m->getall($config['per_page'], $offset);
        $this->load->view('theme/layout', $data);
    }

    public function add($id = '') {
        $this->load->library("form_validation");
        $rules = $this->administrator_m->rules_add;
        if ($id) {
            $rules = $this->administrator_m->rules_edit;
        }
        $this->form_validation->set_rules($rules);
        $data['module'] = "administrator";
        if ($this->form_validation->run()) {
            $data = array(
                'UserName' => $this->input->post('UserName'),
                'FirstName' => $this->input->post('FirstName'),
                'LastName' => $this->input->post('LastName'),
                'Email' => $this->input->post('Email'),
                'AccessLevel' => $this->input->post('AccessLevel'),
                'Status' => $this->input->post('Status')
            );

            if ($this->input->post('Password')) {
                $data['Password'] = md5($this->input->post('Password'));
            }

            if ($id) {
                $data['Modified'] = date('Y-m-d H:i:s');
                $this->administrator_m->update($id, $data);
                $this->session->set_flashdata('item', 'Successfully Updated!');
            } else {
                $data['Created'] = date('Y-m-d H:i:s');
                $insert_id=$this->administrator_m->insert($data);
                $this->session->set_flashdata('item', 'Successfully Added!');
            }
            redirect('administrator');
        } else {
            if ($id) {
                $data['admin'] = $this->administrator_m->get($id);
                $data['view'] = "administrator/edit";
                $data['title'] = "Edit Administrator";
            } else {
                $data['view'] = "administrator/add";
                $data['title'] = "Add Administrator";
            }
            $this->load->view('theme/layout', $data);
        }
    }

    public function delete($id = '') {
        $this->administrator_m->delete($id);
        redirect('administrator');
    }

}
