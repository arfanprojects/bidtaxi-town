<?php if ($this->session->userdata('item')): ?>
    <div class="alert alert-block alert-success col-xs-11">
        <button data-dismiss="alert" class="close" type="button">
            <i class="ace-icon fa fa-times"></i>
        </button>

        <i class="ace-icon fa fa-check green"></i>
        <?php echo $this->session->flashdata('item'); ?>
    </div>
<?php endif; ?>
<div class="page-header col-xs-12">
    <h1>
        <?php echo $title; ?>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            <?php echo $title; ?>
        </small>
    </h1>

</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <a style="margin:5px 6px;" href="<?php echo base_url(); ?>administrator/add" class="btn btn btn-primary pull-right"><i class="ace-icon glyphicon glyphicon-plus"></i> Add</a>
                <table class="table  table-bordered table-hover" id="simple-table" style="margin:0;">
                    <thead>
                        <tr>
                            <!--<th class="detail-col">ID</th>-->
                            <th>User Name</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>AccessLevel</th>
                            <th>Status</th>
                            <th>Logged On</th>
                            <th>Logged Off</th>
                            <th>Options</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($administrator as $row): ?>
                            <tr>
<!--                                <td class=" bigger-120 center">

                                    <?php //echo $row->ID_Administrator; ?>

                                </td>-->
                                <td class=" bigger-120 center">

                                    <?php echo $row->UserName; ?>

                                </td>
                                <td class=" bigger-120 center">

                                    <?php echo $row->FirstName; ?> <?php echo $row->LastName; ?>

                                </td>

                                <td class="bigger-120">
                                    <?php echo $row->Email ?>
                                </td>

                                <td class="bigger-120">
                                    <?php
                                    if ($row->AccessLevel == 1) {
                                        echo 'Company Admin';
                                    } elseif ($row->AccessLevel == 2) {
                                        echo 'Driver Admin';
                                    } elseif ($row->AccessLevel == 3) {
                                        echo 'Driver';
                                    }
                                    ?>
                                </td>
                                <td class="bigger-120">
                                    <?php
                                    if ($row->Status == 0) {
                                        echo '<span class="label label-sm label-warning">None</span>';
                                    } elseif ($row->Status == 1) {
                                        echo '<span class="label label-sm label-danger">Logged Off</span>';
                                    } elseif ($row->Status == 2) {
                                        echo '<span class="label label-sm label-success">Logged On</span>';
                                    } else {
                                        echo '<span class="label label-sm label-inverse arrowed-in">Disabled</span>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php echo $row->LoggedOn; ?>
                                </td>
                                <td>
                                    <?php echo $row->LoggedOff; ?>
                                </td>

                                <td>
                                    <div class=" btn-group action-buttons">

                                        <a class="green"  href="<?php echo base_url(); ?>administrator/add/<?php echo $row->ID_Administrator; ?>">
                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                        </a>

                                        <a class="red confirm" href="<?php echo base_url(); ?>administrator/delete/<?php echo $row->ID_Administrator; ?>">
                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                        </a>


                                    </div>


                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-12 text-right" >
                        <?php echo $pagination; ?>
                    </div>
                </div>
            </div><!-- /.span -->
        </div>

    </div>

</div>