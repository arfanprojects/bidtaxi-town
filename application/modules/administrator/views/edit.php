<div class="page-header">
    <h1>
        <?php echo $title; ?>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            <?php echo $title; ?>
        </small>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <?php echo form_open('administrator/add/' . $admin->ID_Administrator); ?>
    <div class="form-group col-xs-12 col-md-6">
        <label  for="form-field-1"> User Name <span style="color:red;"><?php echo form_error('UserName'); ?></span></label>
        <input type="text" name="UserName" value="<?php echo $admin->UserName; ?>" required class="form-control" placeholder="User Name">
    </div>
    <div class="form-group col-xs-12 col-md-6">
        <label  for="form-field-1">	First Name <span style="color:red;"><?php echo form_error('FirstName'); ?></label>
        <input type="text" class="form-control" value="<?php echo $admin->FirstName; ?>" required name="FirstName" placeholder="First Name" id="form-field-1">
    </div>
    <div class="form-group col-xs-12 col-md-6">
        <label  for="form-field-1">	Last Name <span style="color:red;"><?php echo form_error('LastName'); ?></label>
        <input type="text" class="form-control" value="<?php echo $admin->LastName; ?>" required name="LastName" placeholder="Last Name" id="form-field-1">
    </div>
    <div class="form-group col-xs-12 col-md-6">
        <label  for="form-field-1">	Email <span style="color:red;"><?php echo form_error('Email'); ?></label>
        <input type="email" class="form-control" value="<?php echo $admin->Email; ?>" required name="Email" placeholder="Email" id="form-field-1">
    </div>
    <div class="form-group col-xs-12 col-md-6">
        <label  for="form-field-1">	Password <span style="color:red;"><?php echo form_error('Password'); ?></label>
        <input type="password" class="form-control" value=""  name="Password" placeholder="Password" id="form-field-1">
    </div>

    <div class="form-group col-xs-12 col-md-2 col-sm-6">
        <label  for="form-field-1">Access Level <span style="color:red;"><?php echo form_error('AccessLevel'); ?></label>
        <select name="AccessLevel"  class="form-control">
            <option value="1" <?php if ($admin->AccessLevel == 1) {
        echo 'selected';
    } ?>>Company Admin</option>
            <option value="2" <?php if ($admin->AccessLevel == 2) {
        echo 'selected';
    } ?>>Driver Admin</option>
            <option value="3" <?php if ($admin->AccessLevel == 3) {
        echo 'selected';
    } ?>>Driver</option>
        </select>
    </div>
    <div class="form-group col-xs-12 col-md-2 col-sm-6">
        <label  for="form-field-1">Status <span style="color:red;"><?php echo form_error('Status'); ?></label>
        <select name="Status"  class="form-control">
            <option value="0" <?php if ($admin->Status == 0) {
        echo 'selected';
    } ?> >None</option>
            <option value="1" <?php if ($admin->Status == 1) {
        echo 'selected';
    } ?>>Logged Off</option>
            <option value="2" <?php if ($admin->Status == 2) {
        echo 'selected';
    } ?>>Logged On</option>
            <option value="3" <?php if ($admin->Status == 3) {
        echo 'selected';
    } ?>>Disabled</option>
        </select>
    </div>
    <div class="col-xs-12">
        <button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

        &nbsp; &nbsp; &nbsp;
        <a href="<?php echo base_url(); ?>administrator" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>

    </div>

</form>
</div>
