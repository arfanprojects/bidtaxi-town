<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Administrator_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_Administrator';
    protected $_table = 'administrator';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'UserName',
            'label' => 'User Name',
            'rules' => 'required|trim'
        ), array(
            'field' => 'FirstName',
            'label' => 'First Name',
            'rules' => 'required|trim'
        ), array(
            'field' => 'LastName',
            'label' => 'Last Name',
            'rules' => 'required|trim'
        ), array(
            'field' => 'Email',
            'label' => 'Email',
            'rules' => 'required'
        ), array(
            'field' => 'Password',
            'label' => 'Password',
            'rules' => 'required|trim'
        ), array(
            'field' => 'AccessLevel',
            'label' => 'Access Level',
            'rules' => 'required|trim'
        ), array(
            'field' => 'Status',
            'label' => 'Status',
            'rules' => 'required|trim'
        )
    );
    public $rules_edit = array(
        array(
            'field' => 'UserName',
            'label' => 'User Name',
            'rules' => 'required|trim'
        ), array(
            'field' => 'FirstName',
            'label' => 'First Name',
            'rules' => 'required|trim'
        ), array(
            'field' => 'LastName',
            'label' => 'Last Name',
            'rules' => 'required|trim'
        ), array(
            'field' => 'Email',
            'label' => 'Email',
            'rules' => 'required|trim|valid_email'
        ), array(
            'field' => 'AccessLevel',
            'label' => 'Access Level',
            'rules' => 'required|trim'
        ), array(
            'field' => 'Status',
            'label' => 'Status',
            'rules' => 'required|trim'
        )
    );
    public function getall($limit,$offset){
		$this->db->where("ID_Company" , $this->session->userdata("ID_Company"));
        $query=$this->db->get($this->_table, $limit, $offset);
        return $query->result();

    }

}
