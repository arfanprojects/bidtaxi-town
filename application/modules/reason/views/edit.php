<div class="page-header">
	<h1>
		<?php echo $title;?>
	</h1>

</div><!-- /.page-header -->
<div class="col-xs-12 col-sm-6">
<?php echo form_open('reason/save/'.$reason->ID_Reason);?>
<div class="form-group">
<label  for="form-field-1">Reason Type <span style="color:red;"><?php echo form_error('ReasonType');?></span></label>
<select class="form-control chosen-select" id="form-field-select-1" name="ReasonType">
<option value=''>Reason Type</option>

<option value='1' <?php if($reason->ReasonType=="Driver Inactive Reason"){ echo 'selected';}?>>Driver Inactive Reason</option>
<option value='2' <?php if($reason->ReasonType=="Customer Barred Reason"){ echo 'selected';}?>>Customer Barred Reason</option>
</select>
</div>
<div class="form-group">
<label  for="form-field-1">	Country Text <span style="color:red;"><?php echo form_error('ReasonText');?></label>
<input type="text" class="form-control" value="<?php echo $reason->ReasonText;?>" name="ReasonText" placeholder="Country Text" id="form-field-1">
</div>
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>reason" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>

</form>
</div>

