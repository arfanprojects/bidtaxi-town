<div class="page-header">
	<h1>
		<?php echo $title;?>
		<!-- <small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Static &amp; Dynamic Tables
		</small> -->
	</h1>

</div><!-- /.page-header -->
<div class="col-xs-12 col-sm-6">
<?php echo form_open('reason/save');?>
<div class="form-group">
<label  for="form-field-1">Reason Type <span style="color:red;"><?php echo form_error('ReasonType');?></span></label>
<select class="form-control chosen-select"  name="ReasonType">
<option value=''>Reason Type</option>
<option value='Driver inactive Reason'>Driver Inactive Reason</option>
<option value='Customer barred Reason'>Customer Barred Reason</option>
</select>
</div>
<div class="form-group">
<label  for="form-field-1">	Reason Text <span style="color:red;"><?php echo form_error('ReasonText');?></label>
<input type="text" class="form-control" name="ReasonText" placeholder="Reason Text" id="form-field-1">
</div>
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>reason" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>

</form>
</div>

