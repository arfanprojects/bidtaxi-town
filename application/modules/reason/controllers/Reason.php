<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reason extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('reason_m');
    }

    public function index() {
        $data['view'] = "reason/home";
        $data['title'] = "Reasons";
        $data['reason'] = $this->reason_m->get_all();
        $this->load->view('theme/layout', $data);
    }
    public function add($id='') {
    	if ($id) {
    	$data['reason'] = $this->reason_m->get($id);
    	$data['view'] = "reason/edit";
        $data['title'] = "Edit Reason";
    	}
    	else
    	{
    	$data['view'] = "reason/add";
        $data['title'] = "Add Reason";	
    	}
        $this->load->view('theme/layout', $data);
    }
    public function save($id='') {
    	 $this->load->library("form_validation");
    	 $rules = $this->reason_m->rules_add;
    	 $this->form_validation->set_rules($rules);
         if ($this->form_validation->run() == TRUE) {
        $data=array(
        	'ReasonType' => $this->input->post('ReasonType'),
            'ReasonText' => $this->input->post('ReasonText'),
			'ID_Company' => $this->session->userdata("ID_Company")
        	);
        if($id){
			$this->reason_m->update($id,$data);
			$this->session->set_flashdata('item','Successfully Updated!');
        }
        else
        {
			$this->reason_m->insert($data);
			$this->session->set_flashdata('item','Successfully Added!');
        }
        
			redirect('reason');
	    }
	    else
	    {
	    	$data['view'] = "reason/add";
	        $data['title'] = "Add Reason";
	        $this->load->view('theme/layout', $data);
	    }
    

}
public function delete($id=''){
    	$this->reason_m->delete($id);
    	redirect('reason');
    }

}