
<div class="page-header">
	<h1>
		<?php echo $title;?>
		<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		<?php echo $title;?>
	</small>
	</h1>

</div><!-- /.page-header -->
<div class="col-xs-12 col-sm-6">
<?php echo form_open('driverpayment/add/'.$driverpayment->ID_DriverPayment);?>
<div class="row">
<div class="form-group col-sm-6">
<label>	Driver <span style="color:red;"><?php echo form_error('ID_Driver');?></label>
<select name="ID_Driver" class="form-control">
<option value="">Select Driver</option>
<?php foreach($drivers as $row):?>
<option value="<?php echo $row->ID_Driver;?>" <?php if($row->ID_Driver==$driverpayment->ID_Driver){ echo 'selected';}?>><?php echo $row->FirstName.' '.$row->LastName;?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-sm-6">
<label>	Amount <span style="color:red;"><?php echo form_error('Amount');?></label>
<input type="text" class="form-control" value="<?php echo $driverpayment->Amount;?>" required name="Amount" placeholder="Amount">
</div>
<div class="form-group col-sm-6">
<label>	Payment Date <span style="color:red;"><?php echo form_error('PaymentDateTime');?></label>
<input type="text" class="form-control datetimepicker" required name="PaymentDateTime" value="<?php echo $driverpayment->PaymentDateTime;?>" placeholder="Payment Date">
</div>
<div class="form-group col-sm-6">
<label>	Invoice No <span style="color:red;"><?php echo form_error('InvoiceNo');?></label>
<input type="number" class="form-control" value="<?php echo $driverpayment->InvoiceNo;?>" required name="InvoiceNo" placeholder="Invoice No">
</div>

</div>
<div class="col-xs-12 row">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;
<a href="<?php echo base_url();?>driverpayment" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>
</div>
</form>
</div>
