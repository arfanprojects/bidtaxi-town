<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Driverpayment extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('driverpayment_m');
        $this->load->model('drivers/drivers_m');
    }

    public function index() {
        $data['view'] = "driverpayment/home";
        $data['title'] = "Driver Payment";
		if($this->session->userdata("AccessLevel") == 3) {
			$data['driverpayment'] = $this->driverpayment_m->get_driverpayment($this->session->userdata("user_id"));
		} else {
        $data['driverpayment'] = $this->driverpayment_m->get_payment();
		}
        $this->load->view('theme/layout', $data);
    }
    
    public function add($id='') {
    	 $this->load->library("form_validation");
    	 $rules = $this->driverpayment_m->rules_add;
    	 $this->form_validation->set_rules($rules);
         if ($this->form_validation->run() == TRUE) {
        $data=array(
            'ID_Driver' =>$this->input->post('ID_Driver'),
            'PaymentDateTime' =>$this->input->post('PaymentDateTime'),
            'InvoiceNo' =>$this->input->post('InvoiceNo'),
            'Amount' =>$this->input->post('Amount')
        	);
        if($id){
        $this->driverpayment_m->update($id,$data);
        $this->session->set_flashdata('item','Successfully Updated!');
        }
        else
        {
        $this->driverpayment_m->insert($data);
        $this->session->set_flashdata('item','Successfully Added!');
        }
        
        redirect('driverpayment');
	    }
	    else
	    {
        $data['drivers'] = $this->drivers_m->get_many_by("ID_Company" , $this->session->userdata("ID_Company"));
	    if ($id) {
        $data['driverpayment'] = $this->driverpayment_m->get($id);
        $data['view'] = "driverpayment/edit";
        $data['title'] = "Edit Driver Payment";
        }
        else
        {
        $data['view'] = "driverpayment/add";
        $data['title'] = "Add Driver Payment";  
        }
        $this->load->view('theme/layout', $data);
	    }
    

}
public function delete($id=''){
    	$this->driverpayment_m->delete($id);
    	redirect('driverpayment');
    }

}