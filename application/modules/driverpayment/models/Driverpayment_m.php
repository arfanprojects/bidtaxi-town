<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Driverpayment_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_DriverPayment';
    protected $_table = 'driverpayment';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'ID_Driver',
            'label' => 'Driver',
            'rules' => 'required'
        ),array(
            'field' => 'PaymentDateTime',
            'label' => 'Payment Date',
            'rules' => 'required'
        ),array(
            'field' => 'InvoiceNo',
            'label' => 'Invoice No',
            'rules' => 'required'
        ),array(
            'field' => 'Amount',
            'label' => 'Amount',
            'rules' => 'required'
        )
    );
    public function get_payment(){
        $query=$this->db->select('*')->from('driverpayment as dp')
                        ->join('driver as d','d.ID_Driver=dp.ID_Driver','left')
						->where('d.ID_Company' , $this->session->userdata("ID_Company"))
                        ->get();
                        return $query->result();
    }
	
	public function get_driverpayment($ID_Driver){
        $query=$this->db->select('*')->from('driverpayment as dp')
                        ->join('driver as d','d.ID_Driver=dp.ID_Driver','left')
						->where('dp.ID_Driver' , $ID_Driver)
                        ->get();
                        return $query->result();
    }
}
