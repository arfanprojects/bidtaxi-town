<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Driverbilling_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_DriverBilling';
    protected $_table = 'driverbilling';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'ID_Job',
            'label' => 'Job',
            'rules' => 'required'
        ),array(
            'field' => 'ID_Driver',
            'label' => 'Driver',
            'rules' => 'required'
        ),array(
            'field' => 'JobDateTime',
            'label' => 'Booking Date',
            'rules' => 'required'
        ),array(
            'field' => 'InvoiceNo',
            'label' => 'Invoice No',
            'rules' => 'required'
        ),array(
            'field' => 'Amount',
            'label' => 'Amount',
            'rules' => 'required'
        ),array(
            'field' => 'Tax',
            'label' => 'Tax',
            'rules' => 'required'
        ),array(
            'field' => 'PaymentType',
            'label' => 'Payment Type',
            'rules' => 'required'
        ),array(
            'field' => 'CardNumber',
            'label' => 'Card Number',
            'rules' => 'required'
        ),array(
            'field' => 'InvoiceDate',
            'label' => 'Invoice Date',
            'rules' => 'required'
        )
    );
    public function get_billing(){
        $query=$this->db->select('*')->from('driverbilling as db')
                        ->join('job','job.ID_Job=db.ID_Job','left')
                        ->join('driver as d','d.ID_Driver=db.ID_Driver','left')
                        ->where('d.ID_Company' , $this->session->userdata("ID_Company"))
                        ->get();
                        return $query->result();
    }
	
	public function get_driverbilling($ID_Driver){
        $query=$this->db->select('*')->from('driverbilling as db')
                        ->join('job','job.ID_Job=db.ID_Job','left')
                        ->join('driver as d','d.ID_Driver=db.ID_Driver','left')
                        ->where('db.ID_Driver' , $ID_Driver)
                        ->get();
                        return $query->result();
    }
}
