<?php if ($this->session->userdata('item')):?>
	<div class="alert alert-block alert-success col-xs-11">
	<button data-dismiss="alert" class="close" type="button">
		<i class="ace-icon fa fa-times"></i>
	</button>

	<i class="ace-icon fa fa-check green"></i>
	<?php echo $this->session->flashdata('item');?>
</div>
<?php endif;?>
						<div class="page-header col-xs-12">
							<h1>
								<?php echo $title;?>
								<small>
								<i class="ace-icon fa fa-angle-double-right"></i>
								<?php echo $title;?>
							</small>
							</h1>

						</div><!-- /.page-header -->
						<div class="row">
						<div class="col-xs-12">
							<div class="row">
									<div class="col-xs-12">
										<a style="margin:5px 6px;" href="<?php echo base_url();?>driverbilling/add" class="btn btn btn-primary pull-right"><i class="ace-icon glyphicon glyphicon-plus"></i> Add</a>
										<table class="table  table-bordered table-hover" id="simple-table">
											<thead>
												<tr>
													<th class="detail-col">ID</th>
													<th>Amount</th>
													<th>Tax</th>
													<th>Payment Type</th>
													<th>Card Number</th>
													<th>Job</th>
													<th>Driver</th>
													<th>Options</th>
												</tr>
											</thead>

											<tbody>
												<?php foreach($driverbilling as $row):?>
												<tr>
												
													<td class=" bigger-120 center">
														
												<?php echo $row->ID_DriverBilling;?>
															
													</td>
													<td class="bigger-120">
												<?php echo $row->Amount;?>
													</td>
													<td class="bigger-120">
												<?php echo $row->Tax;?>
													</td>
													<td class="bigger-120">
												<?php if($row->PaymentType==0){
													echo 'None';
												}else if($row->PaymentType==1){
													echo 'Cash';
												}else if($row->PaymentType==2){
													echo 'Card';
												}else if($row->PaymentType==3){
													echo 'PayPal';
												}else if($row->PaymentType==4){
													echo 'ApplePay';
												}else {
													echo 'Other';
												}

												?>
													</td><td class="bigger-120">
												<?php echo $row->CardNumber;?>
													</td><td class="bigger-120">
												<?php echo $row->ID_Job;?>
													</td>
													<td class="bigger-120">
												<?php echo $row->FirstName.' '.$row->LastName;?>
													</td>
													
													
													<td>
														<div class=" btn-group action-buttons">
<?php if($this->session->userdata("AccessLevel") != 3) { ?>
															<a class="green"  href="<?php echo base_url();?>driverbilling/add/<?php echo $row->ID_DriverBilling;?>">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</a>

															<a class="red confirm" href="<?php echo base_url();?>driverbilling/delete/<?php echo $row->ID_DriverBilling;?>">
																<i class="ace-icon fa fa-trash-o bigger-120"></i>
															</a>
															<?php 
															} ?>
															<a class="green" href="<?php echo base_url();?>driverbilling/view/<?php echo $row->ID_DriverBilling;?>">
																<i class="ace-icon fa fa-eye bigger-120"></i>
															</a>

															
														</div>

														
													</td>
												</tr>
											<?php endforeach;?>
											</tbody>
										</table>
									</div><!-- /.span -->
								</div>

						</div>

						</div>