<div class="main-content">
				<div class="main-content-inner">
					

					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">
						<!-- /section:settings.box -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="space-6"></div>

								<div class="row">
									<div class="col-sm-10 col-sm-offset-1">
										<!-- #section:pages/invoice -->
										<div class="widget-box transparent">
											<div class="widget-header widget-header-large">
												<h3 class="widget-title grey lighter">
													<i class="ace-icon fa fa-leaf green"></i>
													Driver Invoice
												</h3>

												<!-- #section:pages/invoice.info -->
												<div class="widget-toolbar no-border invoice-info">
													<span class="invoice-info-label">Invoice:</span>
													<span class="red"><?php echo $invoice->InvoiceNo; ?></span>

													<br />
													<span class="invoice-info-label">Date:</span>
													<span class="blue"><?php echo date("d M y" , strtotime($invoice->InvoiceDate)); ?></span>
												</div>

												<div class="widget-toolbar hidden-480">
													<a href="#">
														<i class="ace-icon fa fa-print"></i>
													</a>
												</div>

												<!-- /section:pages/invoice.info -->
											</div>

											<div class="widget-body">
												<div class="widget-main padding-24">
													<div class="row">
														<div class="col-sm-6">
															<div class="row">
																<div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
																	<b>Company Info</b>
																</div>
															</div>

															<div>
																<ul class="list-unstyled spaced">
																	<li>
																		<i class="ace-icon fa fa-caret-right blue"></i>Street, City
																	</li>

																	<li>
																		<i class="ace-icon fa fa-caret-right blue"></i>Zip Code
																	</li>

																	<li>
																		<i class="ace-icon fa fa-caret-right blue"></i>State, Country
																	</li>

																	<li>
																		<i class="ace-icon fa fa-caret-right blue"></i>
Phone:
																		<b class="red">111-111-111</b>
																	</li>

																	<li class="divider"></li>

																	<li>
																		<i class="ace-icon fa fa-caret-right blue"></i>
																		Paymant Info
																	</li>
																</ul>
															</div>
														</div><!-- /.col -->

														<div class="col-sm-6">
															<div class="row">
																<div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
																	<b>Driver Info</b>
																</div>
															</div>

															<div>
																<ul class="list-unstyled  spaced">
																	<li>
																		<i class="ace-icon fa fa-caret-right green"></i><?php echo $driver->Address1; ?>, <?php echo $driver->Address2; ?>
																	</li>

																	<li>
																		<i class="ace-icon fa fa-caret-right green"></i><?php echo $driver->PostCode; ?>
																	</li>

																

																	<li class="divider"></li>

																	<li>
																		<i class="ace-icon fa fa-caret-right green"></i>
																		Contact Info
																		<?php echo $driver->Email; ?><br> <?php echo $driver->Telephone1; ?>, <?php echo $driver->Telephone2; ?> 
																	</li>
																</ul>
															</div>
														</div><!-- /.col -->
													</div><!-- /.row -->

													<div class="space"></div>

													<div>
														<table class="table table-striped table-bordered">
															<thead>
																<tr>
																	<th>Driver Name</th>
																	<th class="hidden-xs">Job ID</th>
																	<th class="hidden-480">Job Date Time</th>
																	<th>Amount</th>
																</tr>
															</thead>
															
															<tbody>
																<tr>
																	<td class="center"><?php echo $driver->FirstName . " " . $driver->LastName; ?></td>

																	<td>
																		<a href="#"><?php echo $invoice->ID_Job; ?></a>
																	</td>
																	<td class="">
																		<?php echo date("d-m-y h:i A" , strtotime($invoice->JobDateTime)); ?>
																	</td>
																	<td>&pound;<?php echo $invoice->Amount; ?></td>
																	
																</tr>
															</tbody>
														</table>
													</div>

													<div class="hr hr8 hr-double hr-dotted"></div>

													<div class="row">
														<div class="col-sm-5 pull-right">
															<h4 class="pull-right">
																Total amount :
																<span class="red">&pound;<?php echo $invoice->Amount; ?></span>
															</h4>
														</div>
														<div class="col-sm-7 pull-left"> Extra Information </div>
													</div>

													<div class="space-6"></div>
													<div class="well">
														Thank you for choosing BidTaxi.
				We believe you will be satisfied by our services.
													</div>
												</div>
											</div>
										</div>

										<!-- /section:pages/invoice -->
									</div>
								</div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			