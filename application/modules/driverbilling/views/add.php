<div class="page-header">
	<h1>
		<?php echo $title;?>
		<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		<?php echo $title;?>
	</small>
	</h1>

</div><!-- /.page-header -->
<div class="col-xs-12">
<?php echo form_open('driverbilling/add');?>
<div class="row">
<div class="form-group col-sm-3">
<label>	Job <span style="color:red;"><?php echo form_error('ID_Job');?></label>
<select name="ID_Job"  class="form-control">
<option value="">Select Job</option>
<?php foreach($jobs as $row):?>
<option value="<?php echo $row->ID_Job?>"><?php echo $row->ID_Job?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-sm-3">
<label>	Driver <span style="color:red;"><?php echo form_error('ID_Driver');?></label>
<select name="ID_Driver" required class="form-control">
<option value="">Select Driver</option>
<?php foreach($drivers as $row):?>
<option value="<?php echo $row->ID_Driver;?>"><?php echo $row->FirstName.' '.$row->LastName;?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-sm-6">
<label>	Job Date <span style="color:red;"><?php echo form_error('JobDateTime');?></label>
<input type="text" class="form-control datetimepicker" required name="JobDateTime" placeholder="Job Date">
</div>
<div class="form-group col-sm-6">
<label>	Invoice No <span style="color:red;"><?php echo form_error('InvoiceNo');?></label>
<input type="number" class="form-control" required name="InvoiceNo" placeholder="Invoice No">
</div>

<div class="form-group col-sm-6">
<label>	Invoice Date <span style="color:red;"><?php echo form_error('InvoiceDate');?></label>
<input type="text" class="form-control datetimepicker" required name="InvoiceDate" placeholder="Invoice Date">
</div>
<div class="form-group col-sm-6">
<label>	Amount <span style="color:red;"><?php echo form_error('Amount');?></label>
<input type="text" class="form-control" required name="Amount" placeholder="Amount">
</div>
<div class="form-group col-sm-6">
<label>	Tax <span style="color:red;"><?php echo form_error('Tax');?></label>
<input type="text" class="form-control" required name="Tax" placeholder="Tax">
</div>
<div class="form-group col-sm-2">
<label>	Payment Type <span style="color:red;"><?php echo form_error('PaymentType');?></label>
<select name="PaymentType" required class="form-control">
<option value="0">None</option>
<option value="1">Cash</option>
<option value="2">Card</option>
<option value="3">Paypal</option>
<option value="4">ApplePay</option>
<option value="5">Other</option>
</select>
</div>
<div class="form-group col-sm-4">
<label>	Card Number <span style="color:red;"><?php echo form_error('CardNumber');?></label>
<input type="text" class="form-control" required name="CardNumber" placeholder="Card Number">
</div>
</div>
<div class="col-xs-12 row">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>driverbilling" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>



</div></form>
</div>

