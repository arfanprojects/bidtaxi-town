<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Driverbilling extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Driverbilling_m');
        $this->load->model('drivers/drivers_m');
        $this->load->model('jobs/jobs_m');
    }

    public function index() {
        $data['view'] = "driverbilling/home";
        $data['title'] = "Driver Billing";
		if($this->session->userdata("AccessLevel") == 3) {
			$data['driverbilling'] = $this->Driverbilling_m->get_driverbilling($this->session->userdata("user_id"));
		} else {
        $data['driverbilling'] = $this->Driverbilling_m->get_billing();
		}
        $this->load->view('theme/layout', $data);
    }
	
	public function view($id) {
        $data['view'] = "driverbilling/invoice";
        $data['title'] = "Driver Billing Invoice";
        $invoice = $this->Driverbilling_m->get($id);
		$data['invoice'] = $invoice;
        $data['driver'] = $this->drivers_m->get($invoice->ID_Driver);
        $this->load->view('theme/layout', $data);
    }
    
    public function add($id='') {
    	 $this->load->library("form_validation");
    	 $rules = $this->Driverbilling_m->rules_add;
    	 $this->form_validation->set_rules($rules);
         if ($this->form_validation->run() == TRUE) {
        $data=array(
            'ID_Job' =>$this->input->post('ID_Job'),
            'ID_Driver' =>$this->input->post('ID_Driver'),
            'JobDateTime' =>$this->input->post('JobDateTime'),
            'InvoiceNo' =>$this->input->post('InvoiceNo'),
            'Amount' =>$this->input->post('Amount'),
            'Tax' =>$this->input->post('Tax'),
            'PaymentType' =>$this->input->post('PaymentType'),
            'CardNumber' =>$this->input->post('CardNumber'),
            'InvoiceDate' =>$this->input->post('InvoiceDate')
        	);
        if($id){
        $this->Driverbilling_m->update($id,$data);
        $this->session->set_flashdata('item','Successfully Updated!');
        }
        else
        {
        $this->Driverbilling_m->insert($data);
        $this->session->set_flashdata('item','Successfully Added!');
        }
        
        redirect('driverbilling');
	    }
	    else
	    {
        $data['jobs'] = $this->jobs_m->get_all();
        $data['drivers'] = $this->drivers_m->get_many_by("ID_Company" , $this->session->userdata("ID_Company"));
	    if ($id) {
        $data['driverbilling'] = $this->Driverbilling_m->get($id);
        $data['view'] = "driverbilling/edit";
        $data['title'] = "Edit Driver Billing";
        }
        else
        {
        $data['view'] = "driverbilling/add";
        $data['title'] = "Add Driver Billing";  
        }
        $this->load->view('theme/layout', $data);
	    }
    

}
public function delete($id=''){
    	$this->Driverbilling_m->delete($id);
    	redirect('driverbilling');
    }

}