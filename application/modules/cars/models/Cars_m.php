<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      M Arfan 
 * @copyright   (c) 2014, CMS Development
 * @since       Version 0.1
 */
class Cars_m extends MY_Model {

    // table name and rules defined for login form 
    protected $primary_key = 'ID_Car';
    protected $_table = 'car';
    public function __construct() {
        parent::__construct();
    }
    public $rules_add = array(
        array(
            'field' => 'driver',
            'label' => 'Driver',
            'rules' => 'required|trim'
        ), array(
            'field' => 'Make',
            'label' => 'Car Maker',
            'rules' => 'required|trim'
        ), array(
            'field' => 'Model',
            'label' => 'Model',
            'rules' => 'required|trim'
        ), array(
            'field' => 'Colour',
            'label' => 'Colour',
            'rules' => 'required|trim'
        ), array(
            'field' => 'RegistrationPlate',
            'label' => 'Registration Plate',
            'rules' => 'required|trim'
        ), array(
            'field' => 'PIN',
            'label' => 'PIN',
            'rules' => 'required|trim'
        ), array(
            'field' => 'RegistrationDate',
            'label' => 'Registration Date',
            'rules' => 'required|trim'
        ), array(
            'field' => 'PassengerSeats',
            'label' => 'Passenger Seats',
            'rules' => 'required|trim'
         )
        // , array(
        //     'field' => 'Category',
        //     'label' => 'Category',
        //     'rules' => 'required|trim'
        // )
            , array(
            'field' => 'OperatingPlate',
            'label' => 'Operating Plate',
            'rules' => 'required|trim'
          )        
    );
}
