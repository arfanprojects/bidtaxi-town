<script>
  $(function() {
  	$('button[type="submit"]').prop('disabled',true);
  	/*$(document).on('keypress','#PassengerSeats',function(){
      var seats=$('#PassengerSeats').val();
      if ($.isNumeric(seats)) {
        return true;
      }
      else{
      	$('#PassengerSeats').val('');
      	return false;
      }

  	}); */
	
  	$(document).on('change','#driver',function(){
         var driver= $(this).val();
         if (driver!=='') {
         	$('button[type="submit"]').prop('disabled',false);
         }else{
			alert("Please Select a Driver")
         	//$('button[type="submit"]').prop('disabled',true);
         	return false;
         }
  			});
});
</script>
<div class="page-header">
	<h1>
		<?php echo $title;?>
		<small>
		<i class="ace-icon fa fa-angle-double-right"></i>
		<?php echo $title;?>
	</small>
	</h1>

</div><!-- /.page-header -->
<?php echo form_open_multipart('cars/save');?>
<div class="col-xs-12 col-sm-6">
<div class="row">
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">Driver <span style="color:red;"><?php echo form_error('driver');?></span></label>
<select class="form-control chosen-select" required id="driver" name="driver">
	<option value="">Select Driver</option>
<?php foreach($drivers as $row):?>
	<option value="<?php echo $row->ID_Driver;?>"><?php echo $row->FirstName; echo $row->LastName;?></option>
<?php endforeach;?>
</select>
</div>
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Make <span style="color:red;"><?php echo form_error('Make');?></label>
<input type="text" class="form-control" required name="Make" placeholder="Make">
</div>
</div>
</div>
<div class="col-xs-12 col-sm-6">
<div class="row">
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Model <span style="color:red;"><?php echo form_error('Model');?></label>
<input type="text" class="form-control" required name="Model" placeholder="Model">
</div>
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Colour <span style="color:red;"><?php echo form_error('Colour');?></label>
<input type="text" class="form-control" required name="Colour" placeholder="Colour">
</div>
</div>
</div>
<div class="col-xs-12 col-sm-6">
<div class="row">
	<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	PIN <span style="color:red;"><?php echo form_error('PIN');?></label>
<input type="text" class="form-control" required name="PIN" placeholder="PIN">
</div>
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Registration Plate <span style="color:red;"><?php echo form_error('RegistrationPlate');?></label>
<input type="text" class="form-control" required name="RegistrationPlate" placeholder="Registration Plate">
</div>

</div>
</div>
<div class="col-xs-12 col-sm-6">
<div class="row">
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Registration Date <span style="color:red;"><?php echo form_error('RegistrationDate');?></label>
<input type="text" class="form-control datetimepicker" required name="RegistrationDate" placeholder="Registration Date">
</div>
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Passenger Seats <span style="color:red;"><?php echo form_error('PassengerSeats');?></label>
<input type="number" class="form-control" id="PassengerSeats"  name="PassengerSeats" value="" placeholder="Passenger Seats">
</div>
</div>
</div>
<div class="col-xs-12 col-sm-6">
<div class="row">
<!-- <div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Registration Date <span style="color:red;"><?php echo form_error('RegistrationDate');?></label>
<input type="text" class="form-control datetimepicker" required name="RegistrationDate" placeholder="Registration Date">
</div> -->
<!-- <div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Category <span style="color:red;"><?php echo form_error('Category');?></label>
<select name="Category" class="form-control">
<option value="">Select Category</option>
<option value=""></option>

</select>
</div> -->

<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Operating Plate <span style="color:red;"><?php echo form_error('OperatingPlate');?></label>
<input type="text" class="form-control" name="OperatingPlate" required placeholder="Operating Plate">
</div>
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Expiry Date <span style="color:red;"><?php echo form_error('OperatingPlateExpDate');?></label>
<input type="text" class="form-control datetimepicker" required name="OperatingPlateExpDate" placeholder="Plate Expiry Date">
</div>
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Insurance Expiry Date <span style="color:red;"><?php echo form_error('InsuranceExpDate');?></label>
<input type="text" class="form-control datetimepicker" required name="InsuranceExpDate" placeholder="Insurance Expiry Date">
</div>
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Insurance Type <span style="color:red;"><?php echo form_error('InsuranceType');?></label>
<select name="InsuranceType" class="form-control" required>
<option value="0">Unknown</option>
<option value="1">Third Party</option>
<option value="2">Third Party Fire & Theft</option>
<option value="3">Fully Comprehensive</option>
<option value="4">Fleet Insurance</option>
</select>
</div>

</div>
</div>
<div class="col-xs-12 col-sm-6">
<div class="row">
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	MOT Expiry Date <span style="color:red;"><?php echo form_error('MOTExpDate');?></label>
<input type="text" class="form-control datetimepicker" required name="MOTExpDate" placeholder="MOT Expiry Date">
</div>
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Date VED Expires <span style="color:red;"><?php echo form_error('TaxExpDate');?></label>
<input type="text" class="form-control datetimepicker" required name="TaxExpDate" placeholder="Date VED Expires">
</div>
</div>
</div>
<div class="form-group col-xs-12 col-sm-6">
<label  for="form-field-1">	Insurance Provider <span style="color:red;"><?php echo form_error('InsuranceProvider');?></label>
<input type="text" class="form-control" required name="InsuranceProvider" placeholder="Insurance Provider">
</div>
<div class="form-group col-xs-12 col-sm-6">
					<input type="file" name="upload_file1" id="upload_file1"/> 
                    <div id="moreImageUpload"></div>
                    <div id="moreImageUploadLink" style="display:none;margin-left: 10px;">
                        <a href="javascript:void(0);" id="attachMore">Add More</a>
                    </div>
</div>
					
<div class="col-xs-12 text-right">
<button type="submit" class="btn btn-primary"><i class="ace-icon fa fa-check white"></i> Save</button>

&nbsp; &nbsp; &nbsp;

<a href="<?php echo base_url();?>cars" class="btn btn-danger"><i class="ace-icon fa fa-times white"></i> Cancel</a>

</div>
</form>


<script> 

 $(document).ready(function () {
        $("input[id^='upload_file']").each(function () {
            var id = parseInt(this.id.replace("upload_file", ""));
            $("#upload_file" + id).change(function () {
                if ($("#upload_file" + id).val() !== "") {
                    $("#moreImageUploadLink").show();
                }
            });
        });
    });
	
 $(document).ready(function () {
        var upload_number = 2;
        $('#attachMore').click(function () {
            //add more file
            var moreUploadTag = '';
            moreUploadTag += '<input type="file" id="upload_file' + upload_number + '" name="upload_file' + upload_number + '"/>';
            moreUploadTag += '&nbsp;<a href="javascript:del_file(' + upload_number + ')" style="cursor:pointer;" onclick="return confirm(\"Are you really want to delete ?\")">Delete </a></div>';
            $('<dl id="delete_file' + upload_number + '">' + moreUploadTag + '</dl>').fadeIn('slow').appendTo('#moreImageUpload');
            upload_number++;
        });
    });

    function del_file(eleId) {
        var ele = document.getElementById("delete_file" + eleId);
        ele.parentNode.removeChild(ele);
    }




</script>


