<?php if ($this->session->flashdata('item')):?>
	<div class="alert alert-block alert-success col-xs-11">
	<button data-dismiss="alert" class="close" type="button">
		<i class="ace-icon fa fa-times"></i>
	</button>

	<i class="ace-icon fa fa-check green"></i>
	<?php echo $this->session->flashdata('item');?>
</div>
<?php endif;?>
<?php if ($this->session->flashdata('error')):?>
	<div class="alert alert-block alert-danger col-xs-11">
	<button data-dismiss="alert" class="close" type="button">
		<i class="ace-icon fa fa-times"></i>
	</button>

	<i class="ace-icon fa fa-times red"></i>
	<?php echo $this->session->flashdata('error');?>
</div>
<?php endif;?>
						<div class="page-header col-xs-12">
							<h1>
								<?php echo $title;?>
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									<?php echo $title;?>
								</small>
							</h1>

						</div><!-- /.page-header -->
						<div class="row">
						<div class="col-xs-12">
							<div class="row">
									<div class="col-xs-12">
									<?php 	if($this->session->userdata("AccessLevel")  != 3) { ?>
										<a style="margin:5px 6px;" href="<?php echo base_url();?>cars/add" class="btn btn btn-primary pull-right"><i class="ace-icon glyphicon glyphicon-plus"></i> Add</a>
 <?php } ?>
										<table class="table  table-bordered table-hover" id="simple-table">
											<thead>
												<tr>
													<th class="detail-col">ID</th>
													<th>Images</th>
													<th>Make</th>
													<th>Model</th>
													<th>Colour</th>
													<th>PIN</th>
													<th>Registration Date</th>
													<th>Passenger Seats</th>
													<th>Category</th>
													<th>Options</th>
												</tr>
											</thead>

											<tbody>
												<?php foreach($cars as $row):?>
												<tr>

													<td class=" bigger-120 center">

												<?php echo $row->ID_Car?>

													</td>
													<td>
														
												<?php foreach($row->images as $img): ?>
												<img width="50" height="50" src="<?php echo base_url();?>assets/images/carimage/<?php echo $img->Image;?>">
												<?php endforeach;?>	
													</td>
													<td class=" bigger-120 center">

												<?php echo $row->Make?>

													</td>
													<td class="bigger-120">
												<?php echo $row->Model?>
													</td>
													<td class="bigger-120">
												<?php echo $row->Colour?>
													</td>
													<td class="bigger-120">
												<?php echo $row->PIN;?>
													</td><td class="bigger-120">
												<?php echo date("Y-m-d",strtotime($row->RegistrationDate));?>
													</td><td class="bigger-120">
												<?php echo $row->PassengerSeats;?>
													</td><td class="bigger-120">
												<?php echo $row->Category;?>
													</td>
													<td>
														<div class=" btn-group action-buttons">

															<a class="green "  href="<?php echo base_url();?>cars/add/<?php echo $row->ID_Car;?>">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</a>
	<?php 	if($this->session->userdata("AccessLevel")  != 3) { ?>
															<a class="red confirm" href="<?php echo base_url();?>cars/delete/<?php echo $row->ID_Car;?>">
																<i class="ace-icon fa fa-trash-o bigger-120"></i>
															</a>
															<?php } ?>


														</div>


													</td>
												</tr>
											<?php endforeach;?>
											</tbody>
										</table>
									</div><!-- /.span -->
								</div>

						</div>

						</div>
