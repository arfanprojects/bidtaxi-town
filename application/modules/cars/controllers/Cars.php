<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cars extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('carimage/carimage_m');
        $this->load->model('cars_m');
        $this->load->model('carimage/carimage_m');
        $this->load->model('drivers/drivers_m');
    }

    public function index() {
        $data['view'] = "cars/home";
        $data['title'] = "Cars";
		if($this->session->userdata("AccessLevel") == 1) {
			$cars = $this->cars_m->get_many_by("ID_Company" , $this->session->userdata("ID_Company"));
            foreach ($cars as $row) {
                $row->images=$this->carimage_m->get_many_by('ID_Car',$row->ID_Car);
            }
            $data['cars']=$cars;
		} 
		
		if($this->session->userdata("AccessLevel") == 3) {
			$cars = $this->cars_m->get_many_by(array("ID_Company" => $this->session->userdata("ID_Company"), "ID_Driver" => $this->session->userdata("user_id")));
             foreach ($cars as $row) {
                $row->images=$this->carimage_m->get_many_by('ID_Car',$row->ID_Car);
            }
            $data['cars']=$cars;
		}

        $this->load->view('theme/layout', $data);
    }
    public function add($id='') {

    	if ($id) {
		$data['images'] = $this->carimage_m->get_many_by("ID_Car" , $id);
    	$car = $this->cars_m->get($id);
      $data['car'] = $car;

    if($this->session->userdata("AccessLevel") == 3) {
        $data['drivers'] = $this->drivers_m->get_many_by('ID_Driver' ,  $car->ID_Driver);
      } else {
        $data['drivers'] = $this->drivers_m->get_many_by("ID_Company" , $this->session->userdata("ID_Company"));
      }

    	$data['view'] = "cars/edit";
        $data['title'] = "Edit Car";
    	}
    	else
    	{
    	$data['view'] = "cars/add";
        $data['title'] = "Add Car";
    	}
        $this->load->view('theme/layout', $data);
    }
    public function save($id='') {
    	 $this->load->library("form_validation");
    	 $rules = $this->cars_m->rules_add;
    	 $this->form_validation->set_rules($rules);
         if ($this->form_validation->run() == TRUE) {
        $data=array(
        	'ID_Driver' =>$this->input->post('driver'),
            'Make' =>$this->input->post('Make'),
            'Model' =>$this->input->post('Model'),
            'Colour' =>$this->input->post('Colour'),
            'RegistrationPlate' =>$this->input->post('RegistrationPlate'),
            'PIN' =>$this->input->post('PIN'),
            'RegistrationDate' =>$this->input->post('RegistrationDate'),
            'PassengerSeats' =>$this->input->post('PassengerSeats'),
            'Category' =>$this->input->post('Category'),
            'OperatingPlate' =>$this->input->post('OperatingPlate'),
            'OperatingPlateExpDate' =>$this->input->post('OperatingPlateExpDate'),
            'MOTExpDate' =>$this->input->post('MOTExpDate'),
            'TaxExpDate' =>$this->input->post('TaxExpDate'),
            'InsuranceExpDate' =>$this->input->post('InsuranceExpDate'),
            'InsuranceType' =>$this->input->post('InsuranceType'),
            'InsuranceProvider' =>$this->input->post('InsuranceProvider'),
			'ID_Company' => $this->session->userdata("ID_Company")
        	);
        if($id){
            
        $this->cars_m->update($id,$data);
		if (file_exists($_FILES['upload_file1']['tmp_name'])) {
                foreach ($_FILES as $key => $value) {
                    if ($key != "file1") {
                        $img_res = $this->add_image($key);
                        $image_data = array(
                             'ID_Car' => $id,
                            'Image' => $img_res['name'].'_o.'.$img_res['ext']
                        );
                        $this->carimage_m->insert($image_data);
                    }
                }
            }

        $this->session->set_flashdata('item','Successfully Updated!');
        if($this->session->userdata("AccessLevel") == 3) {

          redirect("cars");
        }

        }
        else
        {
         if ($this->cars_m->get_by('RegistrationPlate',$this->input->post('RegistrationPlate'))) {
               $this->session->set_flashdata('error','Registration plate already in use');
             redirect('cars');
         }
        $insert_id =  $this->cars_m->insert($data);

	   if (file_exists($_FILES['upload_file1']['tmp_name'])) {
                foreach ($_FILES as $key => $value) {
                    if ($key != "file1") {
                        $img_res = $this->add_image($key);
                        $image_data = array(
                             'ID_Car' => $insert_id,
                            'Image' => $img_res['name'].'_o.'.$img_res['ext']
                        );
                        $this->carimage_m->insert($image_data);
                    }
                }
            }

        $this->session->set_flashdata('item','Successfully Added!');
        }

        redirect('cars');
	    }
	    else
	    {
            //redirect('cars/add');
	    	$data['view'] = "cars/add";
	        $data['title'] = "Add Cars";
	        $this->load->view('theme/layout', $data);
	    }


}
	public function delete($id=''){
    	$this->cars_m->delete($id);
    	redirect('cars');
    }


	public function add_image($key) {
        $time = time() . rand();
        $post_image = array(
            'file' => $_FILES[$key],
            'new_name' => $time . '_o',
            'dst_path' => FCPATH.'/assets/images/carimage/'
        );
        $post_image_response = upload_original($post_image);
        return array('name' => $time, 'ext' => $post_image_response['file_ext']);
    }

	public function deleteimage($id) {
		$carid = $this->uri->segment(4);
		$this->carimage_m->delete($id);
		redirect("cars/add/" . $carid);
	}

}
