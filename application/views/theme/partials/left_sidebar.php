<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<!-- #section:basics/sidebar -->
			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>


				<ul class="nav nav-list">
					<?php if($this->session->userdata("AccessLevel") == 3) : ?>
						<li <?php if($this->uri->segment(1) == "dashboard" or $this->uri->segment(1) == "") : ?> class="active" <?php endif; ?> >
							<a href="<?php echo base_url();?>">
								<i class="menu-icon fa fa-tachometer"></i>
								<span class="menu-text"> Dashboard </span>
							</a>

							<b class="arrow"></b>
						</li>

					<li <?php if($this->uri->segment(1) == "drivers") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>drivers/add/<?php echo $this->session->userdata("user_id");  ?>">
							<i class="menu-icon fa fa-users"></i>
							<span class="menu-text"> Driver  </span>
						</a>

						<b class="arrow"></b>
					</li>


					<li <?php if($this->uri->segment(1) == "cars") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>cars">
							<i class="menu-icon fa fa-car"></i>
							<span class="menu-text"> Cars  </span>
						</a>

						<b class="arrow"></b>
					</li>
					
					<li <?php if($this->uri->segment(1) == "jobs") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>jobs">
							<i class="menu-icon fa fa-car"></i>
							<span class="menu-text"> Jobs  </span>
						</a>

						<b class="arrow"></b>
					</li>
					
					<li <?php if($this->uri->segment(1) == "driverbilling") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>driverbilling">
							<i class="menu-icon fa fa-users"></i>
							<span class="menu-text"> Driver Billing </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li <?php if($this->uri->segment(1) == "driverpayment") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>driverpayment">
							<i class="menu-icon fa fa-users"></i>
							<span class="menu-text"> Driver Payment </span>
						</a>

						<b class="arrow"></b>
					</li>


					<?php endif; ?>


					<?php if($this->session->userdata("AccessLevel") == 1) : ?>
					<li <?php if($this->uri->segment(1) == "dashboard" or $this->uri->segment(1) == "") : ?> class="active" <?php endif; ?> >
						<a href="<?php echo base_url();?>">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li <?php if($this->uri->segment(1) == "administrator") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>administrator">
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text"> Administrator </span>
						</a>

						<b class="arrow"></b>
					</li>
					<?php endif; ?>
					<?php if($this->session->userdata("AccessLevel") == 1 || $this->session->userdata("AccessLevel") == 2 ) : ?>
					<?php  $this->load->model("drivers/drivers_m");
						$pending = $this->drivers_m->count_by(array("ID_Company"=> $this->session->userdata("ID_Company") , "Update_Status" => 3));
						$new_pending = $this->drivers_m->count_by(array("ID_Company"=> $this->session->userdata("ID_Company") , "Update_Status" => 2));
						$active = $this->drivers_m->count_by(array("ID_Company"=> $this->session->userdata("ID_Company") , "Update_Status" => 1));
					?>

					<li <?php if($this->uri->segment(1) == "drivers") : ?> class="open" <?php endif; ?>>
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-pencil-square-o"></i>
							<span class="menu-text"> Drivers  <span style="color:red;"> (<?php echo  $active + $pending + $new_pending; ?>) </span> </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li <?php if($this->uri->segment(2) == "") : ?> class="active" <?php endif; ?>>
								<a href="<?php echo base_url();?>drivers">
									<i class="menu-icon fa fa-caret-right"></i>
									Active <span style="color:red;"> (<?php echo $active; ?>) </span>
								</a>

								<b class="arrow"></b>
							</li>

							<li <?php if($this->uri->segment(2) == "new_pending") : ?> class="active" <?php endif; ?>>
								<a href="<?php echo base_url();?>drivers/new_pending">
									<i class="menu-icon fa fa-caret-right"></i>
									New Pending <span style="color:red;"> (<?php echo $new_pending; ?>) </span>
								</a>

								<b class="arrow"></b>
							</li>

							<li  <?php if($this->uri->segment(2) == "pending") : ?> class="active" <?php endif; ?>>
								<a href="<?php echo base_url();?>drivers/pending">
									<i class="menu-icon fa fa-caret-right"></i>
									Pending <span style="color:red;"> (<?php echo $pending; ?>) </span>
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>

					<li <?php if($this->uri->segment(1) == "driverbilling") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>driverbilling">
							<i class="menu-icon fa fa-users"></i>
							<span class="menu-text"> Driver Billing </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li <?php if($this->uri->segment(1) == "driverpayment") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>driverpayment">
							<i class="menu-icon fa fa-users"></i>
							<span class="menu-text"> Driver Payment </span>
						</a>

						<b class="arrow"></b>
					</li>
					<?php endif; ?>
					<?php if($this->session->userdata("AccessLevel") == 1) : ?>
					<li <?php if($this->uri->segment(1) == "cars") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>cars">
							<i class="menu-icon fa fa-car"></i>
							<span class="menu-text"> Cars </span>
						</a>

						<b class="arrow"></b>
					</li>
					<!-- <li <?php if($this->uri->segment(1) == "customers") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>customers">
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text"> Customers </span>
						</a>

						<b class="arrow"></b>
					</li> -->
					<!-- <li <?php if($this->uri->segment(1) == "reason") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>reason">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Reasons </span>
						</a>

						<b class="arrow"></b>
					</li> -->

					<li <?php if($this->uri->segment(1) == "jobs") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>jobs">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Jobs </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li <?php if($this->uri->segment(1) == "driver_job") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>driver_job">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Driver Jobs </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li <?php if($this->uri->segment(1) == "vehicletype") : ?> class="active" <?php endif; ?>>
						<a href="<?php echo base_url();?>vehicletype">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Vehicle Types </span>
						</a>

						<b class="arrow"></b>
					</li>
					<?php endif; ?>

				</ul><!-- /.nav-list -->

				<!-- #section:basics/sidebar.layout.minimize -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<!-- /section:basics/sidebar.layout.minimize -->
			</div>
