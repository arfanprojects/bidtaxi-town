<div id="navbar" class="navbar navbar-default          ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<!-- #section:basics/sidebar.mobile.toggle -->
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<!-- /section:basics/sidebar.mobile.toggle -->
				<div class="navbar-header pull-left">
					<!-- #section:basics/navbar.layout.brand -->
					<a href="<?php echo base_url();?>" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>


							 <?php
                             $db1=$this->load->database('master', TRUE);
                              $id=$this->session->userdata('ID_Company');
                              $db1->where("ID_Company" , $id);
							  $query1= $db1->get('companies');
							   $company=$query1->row();
							   if ($company) {
							    $db1->where("ID_Authority" , $company->ID_Authority);
							  $query2= $db1->get('authorities');
							   $authority=$query2->row();
							   echo  $company->CompanyName.'('.strtolower($authority->AuthorityName).')';
							   }

							  ?>
						</small>
					</a>

					<!-- /section:basics/navbar.layout.brand -->

					<!-- #section:basics/navbar.toggle -->

					<!-- /section:basics/navbar.toggle -->
				</div>

				<!-- #section:basics/navbar.dropdown -->
				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						

						<!-- #section:basics/navbar.user_menu -->
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<!-- <img class="nav-user-photo" src="<?php echo base_url(); ?>assets/avatars/user.jpg" alt="user Photo" /> -->
								<span class="user-info">
									<small>Welcome,</small>
									<?php echo $this->session->userdata('UserName'); ?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">


								<!-- <li class="divider"></li> -->

								<li>
									<a href="<?php echo base_url(); ?>login/logout">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>

						<!-- /section:basics/navbar.user_menu -->
					</ul>
				</div>

				<!-- /section:basics/navbar.dropdown -->
			</div><!-- /.navbar-container -->
		</div>
