<div class="footer">
				<div class="footer-inner">
					<!-- #section:basics/footer -->
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder"></span>
							BidTaxi <span class="text-info"> <?php $path_parts = pathinfo($_SERVER['REQUEST_URI']);
							$folders = explode('/', $path_parts['dirname']);
echo  ucfirst(str_replace("-", '',$folders[1])); ?></span>  &copy; 2015-<?php echo date("Y"); ?>
						</span>
						&nbsp; &nbsp;
					</div>

					<!-- /section:basics/footer -->
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
