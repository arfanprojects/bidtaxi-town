-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2016 at 12:49 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bidtaxi_london`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `ID_Administrator` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(30) DEFAULT NULL,
  `FirstName` varchar(30) DEFAULT NULL,
  `LastName` varchar(30) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Password` varchar(32) DEFAULT NULL,
  `AccessLevel` tinyint(4) DEFAULT NULL,
  `Status` tinyint(4) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Modified` datetime DEFAULT NULL,
  `LoggedOn` datetime DEFAULT NULL,
  `LoggedOff` datetime DEFAULT NULL,
  `ID_Company` int(11) NOT NULL,
  PRIMARY KEY (`ID_Administrator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`ID_Administrator`, `UserName`, `FirstName`, `LastName`, `Email`, `Password`, `AccessLevel`, `Status`, `Created`, `Modified`, `LoggedOn`, `LoggedOff`, `ID_Company`) VALUES
(2, 'adminlondon', 'London Company', NULL, 'admin@london.com', '0192023a7bbd73250516f069df18b500', 1, NULL, NULL, NULL, NULL, NULL, 11),
(3, 'driver', 'Test', 'Test', 'test@test.com', 'e10adc3949ba59abbe56e057f20f883e', 3, NULL, NULL, NULL, NULL, NULL, 11),
(4, 'newdriver', 'New', 'Driver', 'new@driver.com', 'e10adc3949ba59abbe56e057f20f883e', 2, NULL, NULL, NULL, NULL, NULL, 11),
(5, 'drivertest', 'drivertest', 'drivertest', 'drivertest@test.com', 'dbbed8df4d4bf356f15518ba4bdc243a', 3, NULL, NULL, NULL, NULL, NULL, 11),
(8, 'ismail', 'ismail', 'ismail', 'ismail.lhr@gmail.com', 'f3b32717d5322d7ba7c505c230785468', 3, NULL, NULL, NULL, NULL, NULL, 11),
(10, 'user1', 'shd', 'shd', 'hsd@hdf.com', '24c9e15e52afc47c225b757e7bee1f9d', 3, NULL, NULL, NULL, NULL, NULL, 11),
(11, 'user2', 'jsdn', 'shd', 'hsd@hd.om', '7e58d63b60197ceb55a1c487989a3720', 3, NULL, NULL, NULL, NULL, NULL, 11);

-- --------------------------------------------------------

--
-- Table structure for table `archive_log`
--

CREATE TABLE IF NOT EXISTS `archive_log` (
  `ID_ArchiveLog` int(11) NOT NULL AUTO_INCREMENT,
  `DateArchived` datetime DEFAULT NULL,
  `DateTimeCreated` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_ArchiveLog`),
  KEY `idx_Archived` (`DateArchived`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bidlog`
--

CREATE TABLE IF NOT EXISTS `bidlog` (
  `ID_BidLog` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Driver` int(11) DEFAULT NULL,
  `ID_Job` bigint(20) DEFAULT NULL,
  `BidDateTime` datetime DEFAULT NULL,
  `Amount` decimal(19,4) DEFAULT NULL,
  `Successful` tinyint(1) DEFAULT NULL,
  `NoOfBidders` tinyint(3) unsigned DEFAULT NULL,
  `Latitude` double DEFAULT NULL,
  `Longitude` double DEFAULT NULL,
  PRIMARY KEY (`ID_BidLog`),
  KEY `idx_BidLogDriver` (`ID_Driver`,`BidDateTime`),
  KEY `idx_BidLogDateTime` (`BidDateTime`),
  KEY `idx_BidLogSuccessful` (`Successful`,`BidDateTime`),
  KEY `idx_BidLogJob` (`ID_Job`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE IF NOT EXISTS `car` (
  `ID_Car` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Driver` int(11) DEFAULT NULL,
  `Make` varchar(30) DEFAULT NULL,
  `Model` varchar(30) DEFAULT NULL,
  `Colour` varchar(30) DEFAULT NULL,
  `RegistrationPlate` varchar(30) DEFAULT NULL,
  `PIN` smallint(6) DEFAULT NULL,
  `RegistrationDate` datetime DEFAULT NULL,
  `PassengerSeats` tinyint(3) unsigned DEFAULT NULL,
  `Category` tinyint(3) unsigned DEFAULT NULL,
  `OperatingPlate` varchar(20) DEFAULT NULL,
  `OperatingPlateExpDate` datetime DEFAULT NULL,
  `MOTExpDate` datetime DEFAULT NULL,
  `TaxExpDate` datetime DEFAULT NULL,
  `InsuranceExpDate` datetime DEFAULT NULL,
  `InsuranceType` tinyint(3) unsigned DEFAULT NULL,
  `InsuranceProvider` varchar(30) DEFAULT NULL,
  `ID_Company` int(11) NOT NULL,
  PRIMARY KEY (`ID_Car`),
  KEY `idx_Car_Driver` (`ID_Driver`),
  KEY `idx_Car_Reg` (`RegistrationPlate`),
  KEY `idx_Car_VehicleType` (`Category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`ID_Car`, `ID_Driver`, `Make`, `Model`, `Colour`, `RegistrationPlate`, `PIN`, `RegistrationDate`, `PassengerSeats`, `Category`, `OperatingPlate`, `OperatingPlateExpDate`, `MOTExpDate`, `TaxExpDate`, `InsuranceExpDate`, `InsuranceType`, `InsuranceProvider`, `ID_Company`) VALUES
(7, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11),
(8, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11),
(9, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11),
(11, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11),
(12, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11),
(13, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11),
(14, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11),
(15, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11);

-- --------------------------------------------------------

--
-- Table structure for table `carimage`
--

CREATE TABLE IF NOT EXISTS `carimage` (
  `ID_Image` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Car` int(11) DEFAULT NULL,
  `ImageType` smallint(6) DEFAULT NULL,
  `ImageSequence` smallint(6) DEFAULT NULL,
  `Image` longblob,
  PRIMARY KEY (`ID_Image`),
  KEY `idx_carimage_ID_Car_ImageType_ImageSequence` (`ID_Car`,`ImageType`,`ImageSequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `ID_Customer` int(11) NOT NULL,
  `FirstName` varchar(30) DEFAULT NULL,
  `LastName` varchar(30) DEFAULT NULL,
  `Gender` tinyint(3) unsigned DEFAULT NULL,
  `DateOfBirth` datetime DEFAULT NULL,
  `TelephoneNo` varchar(22) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `BarredReason` tinyint(3) unsigned DEFAULT NULL,
  `DateTimeLastUsed` datetime DEFAULT NULL,
  `DateTimeFirstUsed` datetime DEFAULT NULL,
  `DateTimeLastConnected` datetime DEFAULT NULL,
  `PID` int(11) DEFAULT NULL,
  `ID_Company` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`ID_Customer`, `FirstName`, `LastName`, `Gender`, `DateOfBirth`, `TelephoneNo`, `Email`, `BarredReason`, `DateTimeLastUsed`, `DateTimeFirstUsed`, `DateTimeLastConnected`, `PID`, `ID_Company`) VALUES
(1, 'Arfan', 'Ali', 1, '2016-02-09 12:00:00', '43434', 'test@tst.com', 1, NULL, NULL, NULL, NULL, 3),
(1, 'Arfan', 'Ali', 1, '2016-02-09 12:00:00', '43434', 'test@tst.com', 1, NULL, NULL, NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `customerhistory`
--

CREATE TABLE IF NOT EXISTS `customerhistory` (
  `ID_CustHistory` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Customer` int(11) DEFAULT NULL,
  `Pickup` tinyint(1) DEFAULT NULL,
  `Address` varchar(150) DEFAULT NULL,
  `AddressShort` varchar(10) DEFAULT NULL,
  `Latitude` double DEFAULT NULL,
  `Longitude` double DEFAULT NULL,
  `TimesUsed` smallint(6) DEFAULT NULL,
  `DateTimeCreated` datetime DEFAULT NULL,
  `DateTimeLastUsed` datetime DEFAULT NULL,
  `DateTimeHistory1` datetime DEFAULT NULL,
  `DateTimeHistory2` datetime DEFAULT NULL,
  `DateTimeHistory3` datetime DEFAULT NULL,
  `DateTimeHistory4` datetime DEFAULT NULL,
  `DateTimeHistory5` datetime DEFAULT NULL,
  `DateTimeHistory6` datetime DEFAULT NULL,
  `DateTimeHistory7` datetime DEFAULT NULL,
  `DateTimeHistory8` datetime DEFAULT NULL,
  `DateTimeHistory9` datetime DEFAULT NULL,
  `DateTimeHistory10` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_CustHistory`),
  KEY `idx_CustomerAddress` (`ID_Customer`,`AddressShort`),
  KEY `idx_Customer` (`ID_Customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `databaseerrorlog`
--

CREATE TABLE IF NOT EXISTS `databaseerrorlog` (
  `ID_ErrorLog` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(30) DEFAULT NULL,
  `DataTable` varchar(30) DEFAULT NULL,
  `DateTime` datetime DEFAULT NULL,
  `FieldName` varchar(30) DEFAULT NULL,
  `RecordID` bigint(20) DEFAULT NULL,
  `Reading` tinyint(1) DEFAULT NULL,
  `ErrorCode` int(11) DEFAULT NULL,
  `Message` varchar(200) DEFAULT NULL,
  `Data` varbinary(200) DEFAULT NULL,
  PRIMARY KEY (`ID_ErrorLog`),
  KEY `IX_DatabaseErrorLogUser` (`UserName`,`DateTime`),
  KEY `IX_DatabaseErrorLogTable` (`DataTable`,`DateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE IF NOT EXISTS `driver` (
  `ID_Driver` int(11) NOT NULL,
  `FirstName` varchar(30) DEFAULT NULL,
  `LastName` varchar(30) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Address1` varchar(50) DEFAULT NULL,
  `Address2` varchar(50) DEFAULT NULL,
  `Address3` varchar(50) DEFAULT NULL,
  `Address4` varchar(50) DEFAULT NULL,
  `PostCode` varchar(10) DEFAULT NULL,
  `ID_Country` int(11) DEFAULT NULL,
  `Gender` tinyint(3) unsigned DEFAULT NULL,
  `Nationality` int(11) DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `FinishDate` datetime DEFAULT NULL,
  `ID_Car` int(11) DEFAULT NULL,
  `Status` tinyint(3) unsigned DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `InactiveReason` int(11) DEFAULT NULL,
  `NoOfRatings` int(11) DEFAULT NULL,
  `TotalRating` int(11) DEFAULT NULL,
  `AverageRating` tinyint(3) unsigned DEFAULT NULL,
  `Latitude` double DEFAULT NULL,
  `Longitude` double DEFAULT NULL,
  `CRBExpiry` datetime DEFAULT NULL,
  `DrivingLicenceNo` varchar(20) DEFAULT NULL,
  `DrivingLicenceExpiry` datetime DEFAULT NULL,
  `BadgeNo` varchar(20) DEFAULT NULL,
  `BadgeExpiry` datetime DEFAULT NULL,
  `Telephone1` varchar(22) DEFAULT NULL,
  `Telephone2` varchar(22) DEFAULT NULL,
  `AppBuildVerMaj` smallint(6) DEFAULT NULL,
  `AppBuildVerMin` smallint(6) DEFAULT NULL,
  `AppBuildVerRev` smallint(6) DEFAULT NULL,
  `AppBuildVerBuild` smallint(6) DEFAULT NULL,
  `MinAppBuildVerMaj` smallint(6) DEFAULT NULL,
  `MinAppBuildVerMin` smallint(6) DEFAULT NULL,
  `MinAppBuildVerRev` smallint(6) DEFAULT NULL,
  `MinAppBuildVerBuild` smallint(6) DEFAULT NULL,
  `PaymentExpiry` datetime DEFAULT NULL,
  `BarredExpiry` datetime DEFAULT NULL,
  `ID_AppParameters` smallint(6) DEFAULT NULL,
  `IMEI` varchar(17) DEFAULT NULL,
  `SIM` varchar(20) DEFAULT NULL,
  `BankAccountNo` varchar(40) DEFAULT NULL,
  `PID` int(11) DEFAULT NULL,
  `ID_Company` int(11) DEFAULT NULL,
  `Update_Status` int(3) NOT NULL DEFAULT '2',
  PRIMARY KEY (`ID_Driver`),
  KEY `idx_Driver_Car` (`ID_Car`),
  KEY `idx_Driver_Email` (`Email`),
  KEY `idx_Driver_Name` (`LastName`),
  KEY `idx_Driver_StatusUpdate` (`UpdateDateTime`),
  KEY `idx_Driver_Status_UpdateStatus` (`Status`,`UpdateDateTime`),
  KEY `idx_Driver_Company` (`ID_Company`),
  KEY `FK_Country_idx` (`ID_Country`),
  KEY `FK_Nationality_idx` (`Nationality`),
  KEY `FK_Reason_idx` (`InactiveReason`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`ID_Driver`, `FirstName`, `LastName`, `Email`, `Address1`, `Address2`, `Address3`, `Address4`, `PostCode`, `ID_Country`, `Gender`, `Nationality`, `StartDate`, `FinishDate`, `ID_Car`, `Status`, `UpdateDateTime`, `Active`, `InactiveReason`, `NoOfRatings`, `TotalRating`, `AverageRating`, `Latitude`, `Longitude`, `CRBExpiry`, `DrivingLicenceNo`, `DrivingLicenceExpiry`, `BadgeNo`, `BadgeExpiry`, `Telephone1`, `Telephone2`, `AppBuildVerMaj`, `AppBuildVerMin`, `AppBuildVerRev`, `AppBuildVerBuild`, `MinAppBuildVerMaj`, `MinAppBuildVerMin`, `MinAppBuildVerRev`, `MinAppBuildVerBuild`, `PaymentExpiry`, `BarredExpiry`, `ID_AppParameters`, `IMEI`, `SIM`, `BankAccountNo`, `PID`, `ID_Company`, `Update_Status`) VALUES
(3, 'Test', 'Test', 'test@test.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 1),
(4, 'New', 'Driver', 'new@driver.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 3),
(5, 'drivertest', 'drivertest', 'drivertest@test.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 1),
(7, 'ismail', 'ismail', 'ismail.lhr@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 2),
(8, 'ismail', 'ismail', 'ismail.lhr@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 2),
(9, 'ismail', 'ismail', 'ismaillhr@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 2),
(10, 'shd', 'shd', 'hsd@hdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 2),
(11, 'jsdn', 'shd', 'hsd@hd.om', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 2);

-- --------------------------------------------------------

--
-- Table structure for table `driverbilling`
--

CREATE TABLE IF NOT EXISTS `driverbilling` (
  `ID_DriverBilling` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Driver` int(11) DEFAULT NULL,
  `InvoiceNo` int(11) DEFAULT NULL,
  `JobDateTime` datetime DEFAULT NULL,
  `Amount` decimal(19,4) DEFAULT NULL,
  `Tax` decimal(19,4) DEFAULT NULL,
  `ID_Job` bigint(20) DEFAULT NULL,
  `PaymentType` smallint(6) DEFAULT NULL,
  `CardNumber` varchar(50) DEFAULT NULL,
  `InvoiceDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_DriverBilling`),
  KEY `idx_Driver_DateTime` (`ID_Driver`,`JobDateTime`),
  KEY `idx_Invoice` (`InvoiceNo`,`InvoiceDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `driverimage`
--

CREATE TABLE IF NOT EXISTS `driverimage` (
  `ID_Image` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Driver` int(11) DEFAULT NULL,
  `ImageType` smallint(6) DEFAULT NULL,
  `ImageSequence` smallint(6) DEFAULT NULL,
  `Image` longblob,
  PRIMARY KEY (`ID_Image`),
  KEY `idx_driverimage_ID_Driver_ImageType_ImageSequence` (`ID_Driver`,`ImageType`,`ImageSequence`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `driverimage`
--

INSERT INTO `driverimage` (`ID_Image`, `ID_Driver`, `ImageType`, `ImageSequence`, `Image`) VALUES
(1, 4, 1, 1, 0x313435353133313938363938343835313138365f6f2e676966);

-- --------------------------------------------------------

--
-- Table structure for table `driverjob`
--

CREATE TABLE IF NOT EXISTS `driverjob` (
  `ID_Driver_Job` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Job` bigint(20) DEFAULT NULL,
  `ID_Driver` int(11) DEFAULT NULL,
  `DateTimeBooked` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Driver_Job`),
  KEY `FK_DriverJob_Job` (`ID_Job`),
  KEY `idx_DriverBooked` (`ID_Driver`,`DateTimeBooked`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `driverpayment`
--

CREATE TABLE IF NOT EXISTS `driverpayment` (
  `ID_DriverPayment` int(11) NOT NULL,
  `ID_Driver` int(11) DEFAULT NULL,
  `PaymentDateTime` datetime DEFAULT NULL,
  `InvoiceNo` int(11) DEFAULT NULL,
  `Amount` decimal(19,4) DEFAULT NULL,
  PRIMARY KEY (`ID_DriverPayment`),
  KEY `idx_Driver_Payment` (`ID_Driver`,`PaymentDateTime`),
  KEY `idx_Driver_Invoice` (`ID_Driver`,`InvoiceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gpslog`
--

CREATE TABLE IF NOT EXISTS `gpslog` (
  `ID_GPSLog` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Driver` int(11) DEFAULT NULL,
  `DateTime` datetime DEFAULT NULL,
  `Latitude` double DEFAULT NULL,
  `Longitude` double DEFAULT NULL,
  `Speed` double DEFAULT NULL,
  `Heading` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_GPSLog`),
  KEY `idx_GPSLog_Driver` (`ID_Driver`,`DateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE IF NOT EXISTS `job` (
  `ID_Job` bigint(20) NOT NULL AUTO_INCREMENT,
  `Status` tinyint(3) unsigned DEFAULT NULL,
  `AddressFrom` varchar(200) DEFAULT NULL,
  `AddressTo` varchar(200) DEFAULT NULL,
  `CancelledReason` tinyint(3) unsigned DEFAULT NULL,
  `ID_Customer` int(11) DEFAULT NULL,
  `Multistop` tinyint(3) unsigned DEFAULT NULL,
  `ID_Driver` int(11) DEFAULT NULL,
  `ID_Car` int(11) DEFAULT NULL,
  `VehicleCategory` tinyint(3) unsigned DEFAULT NULL,
  `DateTimeBooked` datetime DEFAULT NULL,
  `BookedFromLatitude` double DEFAULT NULL,
  `BookedFromLongitude` double DEFAULT NULL,
  `BookedToLatitude` double DEFAULT NULL,
  `BookedToLongitude` double DEFAULT NULL,
  `CurrentLatitude` double DEFAULT NULL,
  `CurrentLongitude` double DEFAULT NULL,
  `DateTimeArrived` datetime DEFAULT NULL,
  `ArrivedLatitude` double DEFAULT NULL,
  `ArrivedLongitude` double DEFAULT NULL,
  `DateTimePOB` datetime DEFAULT NULL,
  `POBLatitude` double DEFAULT NULL,
  `POBLongitude` double DEFAULT NULL,
  `DateTimeComplete` datetime DEFAULT NULL,
  `CompleteLatitude` double DEFAULT NULL,
  `CompleteLongitude` double DEFAULT NULL,
  `PassengerQuantity` tinyint(3) unsigned DEFAULT NULL,
  `WaitingTime` smallint(6) DEFAULT NULL,
  `OnRouteTime` smallint(6) DEFAULT NULL,
  `PriceBid` decimal(19,4) DEFAULT NULL,
  `PriceAmendedBid` decimal(19,4) DEFAULT NULL,
  `PriceTip` decimal(19,4) DEFAULT NULL,
  `Mileage` double DEFAULT NULL,
  `Notes` varchar(200) DEFAULT NULL,
  `DriverRating` tinyint(1) DEFAULT NULL,
  `CustomerRating` tinyint(1) DEFAULT NULL,
  `TelephoneNumber` varchar(22) DEFAULT NULL,
  `ID_Company` int(11) NOT NULL,
  PRIMARY KEY (`ID_Job`),
  KEY `FK_Job_Car` (`ID_Car`),
  KEY `idx_Job_Driver_Booked` (`ID_Driver`,`DateTimeBooked`),
  KEY `idx_Job_Customer_Booked` (`ID_Customer`,`DateTimeBooked`),
  KEY `FK_Job_Vehicle_idx` (`VehicleCategory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobfeedback`
--

CREATE TABLE IF NOT EXISTS `jobfeedback` (
  `ID_Feedback` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Job` bigint(20) DEFAULT NULL,
  `ID_Driver` int(11) DEFAULT NULL,
  `ID_Customer` int(11) DEFAULT NULL,
  `DateTime` datetime DEFAULT NULL,
  `Comment` char(200) DEFAULT NULL,
  PRIMARY KEY (`ID_Feedback`),
  KEY `IX_JobDateTime` (`ID_Job`,`DateTime`),
  KEY `IX_DateTime` (`DateTime`),
  KEY `FK_JobFeedback_Driver` (`ID_Driver`),
  KEY `FK_JobFeedback_Customer_idx` (`ID_Customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `ID_Caption` int(11) NOT NULL,
  `English` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID_Caption`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `ID_Log` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(30) DEFAULT NULL,
  `WebUserName` varchar(30) DEFAULT NULL,
  `TableName` varchar(30) DEFAULT NULL,
  `ID_Record` bigint(20) DEFAULT NULL,
  `DateTime` datetime DEFAULT NULL,
  `Mesasge` varchar(30) DEFAULT NULL,
  `Data` varbinary(200) DEFAULT NULL,
  PRIMARY KEY (`ID_Log`),
  KEY `idx_log_TableName_ID_Record` (`TableName`,`ID_Record`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `ID_Profile` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(30) DEFAULT NULL,
  `ProfileName` varchar(30) DEFAULT NULL,
  `ProfileRoot` varchar(30) DEFAULT NULL,
  `ProfileDescription` varchar(30) DEFAULT NULL,
  `ProfileSection` varchar(30) DEFAULT NULL,
  `ProfileKey` varchar(30) DEFAULT NULL,
  `ProfileValue` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID_Profile`),
  KEY `Profile_User` (`UserName`),
  KEY `Profile_User_Name` (`UserName`,`ProfileName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reason`
--

CREATE TABLE IF NOT EXISTS `reason` (
  `ID_Reason` int(11) NOT NULL AUTO_INCREMENT,
  `ReasonType` tinyint(3) unsigned DEFAULT NULL,
  `ReasonText` varchar(30) DEFAULT NULL,
  `ID_Company` int(11) NOT NULL,
  PRIMARY KEY (`ID_Reason`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE IF NOT EXISTS `sms` (
  `ID_SMS` int(11) NOT NULL AUTO_INCREMENT,
  `DateTimeCreated` datetime NOT NULL,
  `MessageBody` varchar(500) DEFAULT NULL,
  `PhoneNumber` varchar(20) DEFAULT NULL,
  `MessageStatus` tinyint(3) NOT NULL,
  `BeingProcessed` tinyint(3) NOT NULL,
  `DateTimeProcessed` datetime DEFAULT NULL,
  `ProcessAttempts` tinyint(3) DEFAULT NULL,
  `DateTimeSucceeded` datetime DEFAULT NULL,
  `FailureReason` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`ID_SMS`),
  KEY `idx_SMS_MessageStatus` (`MessageStatus`,`BeingProcessed`,`ProcessAttempts`,`DateTimeCreated`,`ID_SMS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sysdiagrams`
--

CREATE TABLE IF NOT EXISTS `sysdiagrams` (
  `name` varchar(160) NOT NULL,
  `principal_id` int(11) NOT NULL,
  `diagram_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT NULL,
  `definition` longblob,
  PRIMARY KEY (`diagram_id`),
  UNIQUE KEY `UK_principal_name` (`principal_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vehicletype`
--

CREATE TABLE IF NOT EXISTS `vehicletype` (
  `ID_VehicleType` tinyint(3) unsigned NOT NULL,
  `VehicleDescription` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID_VehicleType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicletype`
--

INSERT INTO `vehicletype` (`ID_VehicleType`, `VehicleDescription`) VALUES
(0, 'test');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bidlog`
--
ALTER TABLE `bidlog`
  ADD CONSTRAINT `FK_BidLog_Driver` FOREIGN KEY (`ID_Driver`) REFERENCES `driver` (`ID_Driver`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FX_BidLog_Job` FOREIGN KEY (`ID_Job`) REFERENCES `job` (`ID_Job`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `car`
--
ALTER TABLE `car`
  ADD CONSTRAINT `FK_Driver` FOREIGN KEY (`ID_Driver`) REFERENCES `driver` (`ID_Driver`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_VehicleType` FOREIGN KEY (`Category`) REFERENCES `vehicletype` (`ID_VehicleType`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `carimage`
--
ALTER TABLE `carimage`
  ADD CONSTRAINT `CarID` FOREIGN KEY (`ID_Car`) REFERENCES `car` (`ID_Car`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customerhistory`
--
ALTER TABLE `customerhistory`
  ADD CONSTRAINT `FX_Customer` FOREIGN KEY (`ID_Customer`) REFERENCES `bidtaxi_master`.`customer` (`ID_Customer`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `driver`
--
ALTER TABLE `driver`
  ADD CONSTRAINT `FK_Company` FOREIGN KEY (`ID_Company`) REFERENCES `bidtaxi_master`.`companies` (`ID_Company`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Country` FOREIGN KEY (`ID_Country`) REFERENCES `bidtaxi_master`.`country` (`ID_Country`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Nationality` FOREIGN KEY (`Nationality`) REFERENCES `bidtaxi_master`.`country` (`ID_Country`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Reason` FOREIGN KEY (`InactiveReason`) REFERENCES `reason` (`ID_Reason`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `driverbilling`
--
ALTER TABLE `driverbilling`
  ADD CONSTRAINT `ID_Driver` FOREIGN KEY (`ID_Driver`) REFERENCES `driver` (`ID_Driver`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `driverimage`
--
ALTER TABLE `driverimage`
  ADD CONSTRAINT `DriverID` FOREIGN KEY (`ID_Driver`) REFERENCES `driver` (`ID_Driver`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `driverjob`
--
ALTER TABLE `driverjob`
  ADD CONSTRAINT `FK_DriverJob_Driver` FOREIGN KEY (`ID_Driver`) REFERENCES `driver` (`ID_Driver`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_DriverJob_Job` FOREIGN KEY (`ID_Job`) REFERENCES `job` (`ID_Job`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `driverpayment`
--
ALTER TABLE `driverpayment`
  ADD CONSTRAINT `FK_DriverID` FOREIGN KEY (`ID_Driver`) REFERENCES `driver` (`ID_Driver`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gpslog`
--
ALTER TABLE `gpslog`
  ADD CONSTRAINT `FK_GPSLog_Driver` FOREIGN KEY (`ID_Driver`) REFERENCES `driver` (`ID_Driver`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `job`
--
ALTER TABLE `job`
  ADD CONSTRAINT `FK_Job_Car` FOREIGN KEY (`ID_Car`) REFERENCES `car` (`ID_Car`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Job_Customer` FOREIGN KEY (`ID_Customer`) REFERENCES `bidtaxi_master`.`customer` (`ID_Customer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Job_Driver` FOREIGN KEY (`ID_Driver`) REFERENCES `driver` (`ID_Driver`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Job_Vehicle` FOREIGN KEY (`VehicleCategory`) REFERENCES `vehicletype` (`ID_VehicleType`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
